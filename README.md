# GTM_Qmorph


## Description
The main function of GTM_Qmorph is to generate a full quad mesh based on the Q-Morph algorithm.
The main theory is based on the following two papers:
Owen, S. J., Staten, M. L., Canann, S. A., & Saigal, S. (1999). Q-Morph: An indirect approach to advancing front quad meshing. International Journal for Numerical Methods in Engineering, 44(9), 1317-1340.
汪攀,张见明,韩磊等.基于带约束前沿推进的四边形网格生成方法.湖南大学学报(自然科学版),2017,44(08):29-34.

And refer to the code in the URL below：
http://cupofcad.com/2021/08/13/demo-netgen-open-cascade-tecnology/
https://gitlab.com/ssv/lessons/-/tree/master/Lesson17_pmc


3rd-party library used include
OpenCASCADE
NetGen
vtk
OpenMesh


for more information please visit
http://analysissitus.org/forum/index.php?threads/a-full-quadrilateral-mesh-generation-method-based-on-the-q-morph-algorithm.304/page-2#post-1887


## License
The project is licensed under the GPL License - see the LICENSE file for details.


## Project status
The following problems still exist in this code:
1.Can't control the number of boundary nodes, so there will be at most one triangular element
2.The quality of generated quadrilateral elements is not high enough, and new algorithms should be developed to improve the quality of quadrilateral elements
3.The stability and robustness of the code need to be improved




