// includes
#include <unordered_set>
#include <algorithm>
#include <cmath>
#include <unordered_map>



// VTK includes
#include <vtkActor.h>
#include <vtkAxes.h>
#include <vtkAxesActor.h>
#include <vtkAutoInit.h>
#include <vtkCamera.h>
#include <vtkCommand.h>
#include <vtkType.h>
#include <vtkDataSetMapper.h>
#include <vtkGenericOpenGLRenderWindow.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkObjectFactory.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkSmartPointer.h>
#include <vtkTransform.h>
#include <vtkTriangle.h>
#include <vtkUnstructuredGrid.h>

VTK_MODULE_INIT(vtkRenderingOpenGL2)
VTK_MODULE_INIT(vtkInteractionStyle)



// OCCT includes
#include <BRep_Builder.hxx>
#include <BRepTools.hxx>
#include <RWStl.hxx>
#include <OSD_Timer.hxx>

// OCCT timer
#define TIMER_NEW \
  OSD_Timer __aux_debug_Timer; \
  Standard_Real __aux_debug_Seconds, __aux_debug_CPUTime; \
  Standard_Integer __aux_debug_Minutes, __aux_debug_Hours;

#define TIMER_RESET \
  __aux_debug_Seconds = __aux_debug_CPUTime = 0.0; \
  __aux_debug_Minutes = __aux_debug_Hours = 0; \
  __aux_debug_Timer.Reset();

#define TIMER_GO \
  __aux_debug_Timer.Start();

#define TIMER_FINISH \
  __aux_debug_Timer.Stop(); \
  __aux_debug_Timer.Show(__aux_debug_Seconds, __aux_debug_Minutes, __aux_debug_Hours, __aux_debug_CPUTime);

#define TIMER_COUT_RESULT_MSG(Msg) \
  { \
    std::cout << "\n=============================================" << std::endl; \
    TCollection_AsciiString ascii_msg(Msg); \
    if ( !ascii_msg.IsEmpty() ) \
    { \
      std::cout << Msg                                             << std::endl; \
      std::cout << "---------------------------------------------" << std::endl; \
    } \
    std::cout << "Seconds:  " << __aux_debug_Seconds               << std::endl; \
    std::cout << "Minutes:  " << __aux_debug_Minutes               << std::endl; \
    std::cout << "Hours:    " << __aux_debug_Hours                 << std::endl; \
    std::cout << "CPU time: " << __aux_debug_CPUTime               << std::endl; \
    std::cout << "=============================================\n" << std::endl; \
  }




// OpenMesh includes
#include <OpenMesh/Core/IO/MeshIO.hh>
#include <OpenMesh/Core/Mesh/PolyMesh_ArrayKernelT.hh>
#include <OpenMesh/Core/Geometry/VectorT.hh>
#include <OpenMesh/Tools/Smoother/SmootherT.hh>
#include <OpenMesh/Tools/Smoother/LaplaceSmootherT.hh>
#include <OpenMesh/Tools/Smoother/JacobiLaplaceSmootherT.hh>

typedef OpenMesh::PolyMesh_ArrayKernelT<>  PolyMesh;




// NetGen includes 
#pragma warning(push,0)

#ifndef OCCGEOMETRY
#define OCCGEOMETRY
#endif

#include <occgeom.hpp>
namespace nglib
{
#include "nglib.h"
}

#pragma warning(pop)








void netGenTriangulation(const TopoDS_Shape& thShape,
	PolyMesh& theMesh,
	std::vector< PolyMesh>& theMeshes);


void openMeshToVTK(vtkSmartPointer<vtkPoints>&   thePoints,
	vtkSmartPointer<vtkPolyData>& thePolyData,
	PolyMesh&                     theMesh);


void updateVtkView(vtkSmartPointer<vtkPoints>&   thePoints,
	vtkSmartPointer<vtkPolyData>& thePolyData,
	vtkSmartPointer<vtkRenderWindow>& theRender,
	PolyMesh&                     theMesh);


void saveToVtkFile(const PolyMesh& theMesh,
	std::string theName);


double getDistance(const PolyMesh& theMesh,
	const PolyMesh::VertexHandle& vh1,
	const PolyMesh::VertexHandle& vh2);


double getAngle(const PolyMesh& theMesh,
	const PolyMesh::VertexHandle& v1,
	const PolyMesh::VertexHandle& v2,
	const PolyMesh::VertexHandle& v3);


double getArea(const PolyMesh& theMesh,
	const PolyMesh::VertexHandle& v1,
	const PolyMesh::VertexHandle& v2,
	const PolyMesh::VertexHandle& v3);
	

double getTriaArea(const PolyMesh& theMesh,
	const PolyMesh::FaceHandle& fh);
	
	
double checkFaceAngle(const PolyMesh& theMesh,
	const PolyMesh::VertexHandle& v1,
	const PolyMesh::VertexHandle& v2,
	const PolyMesh::VertexHandle& v3,
	const PolyMesh::VertexHandle& v4);	
	
	
double getFaceAngle(const PolyMesh& theMesh,
	const PolyMesh::VertexHandle& v1,
	const PolyMesh::VertexHandle& v2,
	const PolyMesh::VertexHandle& v3,
	const PolyMesh::VertexHandle& v4);
	
	
double getTriaQualityFactor(const PolyMesh& theMesh,
	const PolyMesh::FaceHandle& fh);


double getTriaQualityFactor(const PolyMesh& theMesh,
	const PolyMesh::VertexHandle& v1,
	const PolyMesh::VertexHandle& v2,
	const PolyMesh::VertexHandle& v3);


double getQuadQualityFactor(const PolyMesh& theMesh,
	const PolyMesh::VertexHandle& v1,
	const PolyMesh::VertexHandle& v2,
	const PolyMesh::VertexHandle& v3,
	const PolyMesh::VertexHandle& v4);


bool checkVetexConnection(PolyMesh& theMesh,
	const PolyMesh::VertexHandle& vh1,
	const PolyMesh::VertexHandle& vh2);


bool checkIsolatedTriangle(PolyMesh& theMesh);


std::vector<PolyMesh::VertexHandle> sortQuadVertices(const std::vector<PolyMesh::VertexHandle>& theVertices,
	const PolyMesh::VertexHandle& theVertex);


std::vector<PolyMesh::VertexHandle> sortQuadVertices(PolyMesh& theMesh,
	PolyMesh::FaceHandle& theFace,
	PolyMesh::VertexHandle& theVertex);


bool checkRecoveryFeasibility(PolyMesh& theMesh,
	const PolyMesh::VertexHandle& vh1,
	const PolyMesh::VertexHandle& vh2);


void findTriaEdges(PolyMesh& theMesh,
	std::vector<PolyMesh::EdgeHandle>& theEdges);
	
	
void findFaceEdges(PolyMesh& theMesh,
	std::vector<PolyMesh::EdgeHandle>& theEdges);


void getEdgeVertices(PolyMesh& theMesh,
	std::vector<PolyMesh::EdgeHandle>& theEdges,
	std::set<PolyMesh::VertexHandle>& theVertices);


void jacobiLaplaceSmooth(PolyMesh& theMesh);


void equivalencePolyMesh(PolyMesh& theMesh, 
	double    theTol);


bool checkVertexTranslation(PolyMesh& theMesh,
	PolyMesh::VertexHandle& vh);


bool edgeRecoverySequence(PolyMesh& theMesh,
	const PolyMesh::VertexHandle& vh1,
	const PolyMesh::VertexHandle& vh2);


bool edgeRecoveryReverse(PolyMesh& theMesh,
	const PolyMesh::VertexHandle& vh1,
	const PolyMesh::VertexHandle& vh2);


std::vector<PolyMesh::EdgeHandle> getGroupBoundaryEdges(PolyMesh& theMesh,
	std::vector<PolyMesh::EdgeHandle>& theEdges);


PolyMesh::VertexHandle getThirdVertex(PolyMesh& theMesh,
	const PolyMesh::VertexHandle& vh1,
	const PolyMesh::VertexHandle& vh2,
	const std::vector<PolyMesh::EdgeHandle>& theEdges);


std::vector<std::pair<PolyMesh::VertexHandle, double>> getVertexAngleList(PolyMesh& theMesh,
	const PolyMesh::VertexHandle& currentVertex,
	const PolyMesh::VertexHandle& secondVertex,
	const PolyMesh::VertexHandle& thirdVertex);


bool checkTriaByVertex(PolyMesh& mesh,
	const PolyMesh::VertexHandle& v1,
	const PolyMesh::VertexHandle& v2,
	const PolyMesh::VertexHandle& v3);


PolyMesh::FaceHandle getTriaByVertex(PolyMesh& theMesh,
	const PolyMesh::VertexHandle& node1,
	const PolyMesh::VertexHandle& node2,
	const PolyMesh::VertexHandle& node3);


void deleteTriaInQuad(PolyMesh& theMesh,
	const PolyMesh::VertexHandle& vh1,
	const PolyMesh::VertexHandle& vh2,
	const PolyMesh::VertexHandle& vh3,
	const PolyMesh::VertexHandle& vh4);


void mergeBadTriaByQuad(PolyMesh& theMesh, 
	PolyMesh::FaceHandle& theQuad, 
	std::set< PolyMesh::VertexHandle > theVertices);


PolyMesh::VertexHandle getSwapSplitTria(PolyMesh& theMesh,
	const PolyMesh::VertexHandle& currentVH,
	const PolyMesh::VertexHandle& secondVH,
	const PolyMesh::VertexHandle& thirdVH,
	const PolyMesh::VertexHandle& leftVH,
	const PolyMesh::VertexHandle& rightVH,
	double threshAngle1);

PolyMesh::VertexHandle getAlignVertex(PolyMesh& theMesh,
	const PolyMesh::VertexHandle& currentVH,
	const PolyMesh::VertexHandle& secondVH,
	const PolyMesh::VertexHandle& thirdVH);



int checkQuadFeasibility(PolyMesh& theMesh,
	const PolyMesh::VertexHandle& vh1,
	const PolyMesh::VertexHandle& vh2,
	const PolyMesh::VertexHandle& vh3,
	const PolyMesh::VertexHandle& vh4);


void combineSingleTria(PolyMesh& theMesh);



void combineQuadWithInteriorVertex(PolyMesh& theMesh);


bool checkQuadNodes(PolyMesh& theMesh, 
	const PolyMesh::VertexHandle& vh1, 
	const PolyMesh::VertexHandle& vh2);
	
	
void mergeSingleTriaNew(PolyMesh& theMesh);


void autoAddFace(PolyMesh& theMesh,
  const PolyMesh::VertexHandle& vh1,
  const PolyMesh::VertexHandle& vh2,
  const PolyMesh::VertexHandle& vh3);

	
void autoAddFace(PolyMesh& theMesh,
	const PolyMesh::VertexHandle& vh1,
	const PolyMesh::VertexHandle& vh2,
	const PolyMesh::VertexHandle& vh3,
	const PolyMesh::VertexHandle& vh4,
	PolyMesh::FaceHandle& theFH);






void netGenTriangulation(const TopoDS_Shape& thShape,
											 PolyMesh& theMesh,
	                     std::vector< PolyMesh>& theMeshes)
{
	Bnd_Box aabb;
	BRepBndLib::Add(thShape, aabb, false);
	const double diag = std::sqrt(aabb.SquareExtent());
	
#ifdef _DEBUG
	std::cout << "diag of shape: " << diag << std::endl;
#endif // _DEBUG

	netgen::MeshingParameters& mp = netgen::mparam;

	mp.minh = 0.0;
	mp.maxh = 0.03*diag;
	// 当这个参数为真时，Netgen会在划分网格时使用局部尺寸函数进行自适应划分。
	// 局部尺寸函数是一个表达式，用于控制在不同区域内的网格大小，
	// 从而使得整个网格更加均匀，并且在重要的区域中增加网格的密度
	mp.uselocalh = true;
	mp.secondorder = false;
	// 四边形单元为主开关
	// mp.quad = 1;
	// 在一般情况下，grading参数的取值通常介于0.1到10之间，其中1表示平均分布。
	// 选用较小的值可以使网格更加细致，而较大的值则会生成相对较粗的网格。
	// 此外，还需要结合模型的具体特征进行选择。
	mp.grading = 0.3;

	nglib::Ng_Init();

	netgen::OCCGeometry occgeo;
	occgeo.shape = thShape;
	occgeo.changed = 1;
	occgeo.BuildFMap();
	occgeo.CalcBoundingBox();

	netgen::Mesh ngMesh;
	netgen::OCCSetLocalMeshSize(occgeo, ngMesh);
	netgen::OCCFindEdges(occgeo, ngMesh);
	netgen::OCCMeshSurface(occgeo, ngMesh, netgen::MESHCONST_OPTSURFACE);

	const int nbNodes = (int)ngMesh.GetNP();
	const int nbTriangles = (int)ngMesh.GetNSE();

	for (int fidx = 1; fidx <= occgeo.fmap.Extent(); ++fidx)
	{
		PolyMesh faceMesh;
		for (int i = 1; i <= nbNodes; ++i)
		{
			const netgen::MeshPoint& point = ngMesh.Point(netgen::PointIndex(i));
			PolyMesh::VertexHandle vh = faceMesh.add_vertex(PolyMesh::Point(point[0], point[1], point[2]));
		}
		netgen::Array<netgen::SurfaceElementIndex> elemIds;
		ngMesh.GetSurfaceElementsOfFace(fidx, elemIds);
		for (const auto& elemId : elemIds)
		{
			const netgen::Element2d& elem = ngMesh.SurfaceElement(netgen::ElementIndex(elemId+1));
			if (elem.GetNP() == 3)
			{
				PolyMesh::FaceHandle fh = faceMesh.add_face(
					PolyMesh::VertexHandle(elem[0] - 1),
					PolyMesh::VertexHandle(elem[1] - 1),
					PolyMesh::VertexHandle(elem[2] - 1));
			}

			if (elem.GetNP() == 4)
			{
				PolyMesh::FaceHandle fh = faceMesh.add_face(
					PolyMesh::VertexHandle(elem[0] - 1),
					PolyMesh::VertexHandle(elem[1] - 1),
					PolyMesh::VertexHandle(elem[2] - 1),
					PolyMesh::VertexHandle(elem[3] - 1));
			}
		}

		faceMesh.garbage_collection();

		/*std::vector<PolyMesh::EdgeHandle> faceEdges;
		findFaceEdges(faceMesh, faceEdges);

		std::set<PolyMesh::VertexHandle> boundaryVertices;
		getEdgeVertices(faceMesh, faceEdges, boundaryVertices);

		if (boundaryVertices.size() % 2 != 0) {

		}*/

		theMeshes.push_back(faceMesh);
	}

#ifdef _DEBUG
	for (int i = 1; i <= nbNodes; ++i)
	{
		const netgen::MeshPoint& point = ngMesh.Point(netgen::PointIndex(i));
		PolyMesh::VertexHandle vh = theMesh.add_vertex(PolyMesh::Point(point[0], point[1], point[2]));
	}
	for (int i = 1; i <= nbTriangles; ++i)
	{
		const netgen::Element2d& elem = ngMesh.SurfaceElement(netgen::ElementIndex(i));
		if (elem.GetNP() == 3)
		{
			PolyMesh::FaceHandle fh = theMesh.add_face(
				PolyMesh::VertexHandle(elem[0] - 1),
				PolyMesh::VertexHandle(elem[1] - 1),
				PolyMesh::VertexHandle(elem[2] - 1));
		}

		if (elem.GetNP() == 4)
		{
			PolyMesh::FaceHandle fh = theMesh.add_face(
				PolyMesh::VertexHandle(elem[0] - 1),
				PolyMesh::VertexHandle(elem[1] - 1),
				PolyMesh::VertexHandle(elem[2] - 1),
				PolyMesh::VertexHandle(elem[3] - 1));
		}
	}
#endif // _DEBUG




	ngMesh.DeleteMesh();
	nglib::Ng_Exit();

	std::cout << "number of netgen nodes: " << nbNodes << std::endl;
	std::cout << "number of netgen trias: " << nbTriangles << std::endl;
	std::cout << "netgen triangulation finish" << std::endl;
	
#ifdef _DEBUG
	saveToVtkFile(theMesh, "netgen_after.vtk");
#endif // _DEBUG
}



void openMeshToVTK(vtkSmartPointer<vtkPoints>&   thePoints,
	               vtkSmartPointer<vtkPolyData>& thePolyData,
	               PolyMesh&                     theMesh) 
{

	for (PolyMesh::VertexIter v_it = theMesh.vertices_begin(); v_it != theMesh.vertices_end(); ++v_it)
	{
		const PolyMesh::Point& point = theMesh.point(*v_it);
		thePoints->InsertNextPoint(point[0], point[1], point[2]);

	}

	vtkSmartPointer<vtkCellArray> cellArray = vtkSmartPointer<vtkCellArray>::New();

	for (auto fit = theMesh.faces_begin(); fit != theMesh.faces_end(); ++fit)
	{
		auto face_vertices = theMesh.fv_range(*fit);
		vtkSmartPointer<vtkIdList> vtk_face = vtkSmartPointer<vtkIdList>::New();
		for (auto vit = face_vertices.begin(); vit != face_vertices.end(); ++vit)
		{
			vtk_face->InsertNextId(static_cast<vtkIdType>(vit->idx()));
		}
		cellArray->InsertNextCell(vtk_face);

	}

	thePolyData->SetPoints(thePoints);
	thePolyData->SetPolys(cellArray);

#ifdef _DEBUG
	saveToVtkFile(theMesh, "tovtk_after.vtk");
#endif // _DEBUG
}



void updateVtkView(vtkSmartPointer<vtkPoints>&   thePoints,
	vtkSmartPointer<vtkPolyData>& thePolyData,
	vtkSmartPointer<vtkRenderWindow>& theRender,
	PolyMesh&                     theMesh)
{
	thePoints->Reset();
	thePolyData->Reset();

	openMeshToVTK(thePoints, thePolyData, theMesh);

	thePolyData->Modified();

	theRender->Render();
}



void saveToVtkFile(const PolyMesh& theMesh,
	std::string theName)
{
	std::string path = "E:/vtk/";
	std::string savepath = path + theName;
	try
	{
		if (!OpenMesh::IO::write_mesh(theMesh, savepath))
		{
			std::cerr << "Cannot write mesh to file " << theName << std::endl;
			return;
		}
	}
	catch (std::exception& x)
	{
		std::cerr << x.what() << std::endl;
		return;
	}
}







double getDistance(const PolyMesh& theMesh, 
	const PolyMesh::VertexHandle& vh1,
	const PolyMesh::VertexHandle& vh2)
{
	// 获取两个节点的坐标
	OpenMesh::Vec3f p1 = theMesh.point(vh1);
	OpenMesh::Vec3f p2 = theMesh.point(vh2);

	double distace = (p2 - p1).norm();// 计算向量范数

	return distace;
}





double getAngle(const PolyMesh& theMesh, 
	const PolyMesh::VertexHandle& v1,
	const PolyMesh::VertexHandle& v2,
	const PolyMesh::VertexHandle& v3)
{

	// 计算向量v2v1和v2v3
	PolyMesh::Normal v2v1 = theMesh.point(v1) - theMesh.point(v2);
	PolyMesh::Normal v2v3 = theMesh.point(v3) - theMesh.point(v2);

	v2v1.normalize();
	v2v3.normalize();

	double dot = v2v1 | v2v3;
	if (dot > 1.0)
		return 0.0;
	if (dot < -1.0)
		return 180.0;
	double angle = std::acos(dot);

	// 返回角度（弧度）
	return angle * 180.0 / M_PI;
}





double getArea(const PolyMesh& theMesh,
	const PolyMesh::VertexHandle& v1,
	const PolyMesh::VertexHandle& v2,
	const PolyMesh::VertexHandle& v3) 
{
	double d1 = getDistance(theMesh, v1, v2);
	double d2 = getDistance(theMesh, v2, v3);
	double d3 = getDistance(theMesh, v1, v3);
	double d = (d1 + d2 + d3) / 2.0;

	double area = sqrt(d*(d - d1)*(d - d2)*(d - d3));

	return area;
}






double getTriaArea(const PolyMesh& theMesh, 
	const PolyMesh::FaceHandle& fh)
{
	PolyMesh::HalfedgeHandle heh = theMesh.halfedge_handle(fh);
	PolyMesh::VertexHandle vh1 = theMesh.to_vertex_handle(heh);
	PolyMesh::VertexHandle vh2 = theMesh.to_vertex_handle(theMesh.next_halfedge_handle(heh));
	PolyMesh::VertexHandle vh3 = 
		theMesh.to_vertex_handle(theMesh.next_halfedge_handle(theMesh.next_halfedge_handle(heh)));

	double area = getArea(theMesh, vh1, vh2, vh3);
	return area;
}




double checkFaceAngle(const PolyMesh& theMesh,
	const PolyMesh::VertexHandle& v1,
	const PolyMesh::VertexHandle& v2,
	const PolyMesh::VertexHandle& v3,
	const PolyMesh::VertexHandle& v4)
{
	// 计算v1, v2, v3构成的三角形面与v2, v1, v4构成的三角形面之间的夹角
	// 如果v3, v4与v1, v2在同一平面，v3, v4在v1, v2的同一侧返回0，不同侧返回180
	PolyMesh::Point p1 = theMesh.point(v1);
	PolyMesh::Point p2 = theMesh.point(v2);
	PolyMesh::Point p3 = theMesh.point(v3);
	PolyMesh::Point p4 = theMesh.point(v4);

	double jointEdgeLen = getDistance(theMesh, v1, v2);
	double areaThresh = jointEdgeLen * jointEdgeLen * 0.1 * 0.5;
	// 计算两个三角形的面积，如果它们中的任何一个的面积接近于0，则返回0
	double area1 = getArea(theMesh, v1, v2, v3);
	double area2 = getArea(theMesh, v1, v2, v4);
	if (area1 < areaThresh || area2 < areaThresh)
		return -1.0;

	PolyMesh::Normal n1 = (p1 - p3) % (p2 - p3);
	n1.normalize();

	PolyMesh::Normal n2 = (p2 - p4) % (p1 - p4);
	n2.normalize();

	double dot = n1 | n2;
	if (dot > 1.0)
		return 180.0;
	if (dot < -1.0)
		return 0.0;
	double angle = std::acos(dot);
	double rs = 180.0 - angle * 180.0 / M_PI;
	return rs;
}



double getFaceAngle(const PolyMesh& theMesh,
	const PolyMesh::VertexHandle& v1,
	const PolyMesh::VertexHandle& v2,
	const PolyMesh::VertexHandle& v3,
	const PolyMesh::VertexHandle& v4)
{
	// 计算v1, v2, v3构成的三角形面与v2, v1, v4构成的三角形面之间的夹角
	// 如果v3, v4与v1, v2在同一平面，v3, v4在v1, v2的同一侧返回0，不同侧返回180
	PolyMesh::Point p1 = theMesh.point(v1);
	PolyMesh::Point p2 = theMesh.point(v2);
	PolyMesh::Point p3 = theMesh.point(v3);
	PolyMesh::Point p4 = theMesh.point(v4);


	PolyMesh::Normal n1 = (p1 - p3) % (p2 - p3);
	n1.normalize();

	PolyMesh::Normal n2 = (p2 - p4) % (p1 - p4);
	n2.normalize();

	double dot = n1 | n2;
	if (dot > 1.0)
		return 180.0;
	if (dot < -1.0)
		return 0.0;
	double angle = std::acos(dot);
	double rs = 180.0 - angle * 180.0 / M_PI;
	return rs;
}






double getTriaQualityFactor(const PolyMesh& theMesh,
	const PolyMesh::FaceHandle& fh)
{
	PolyMesh::HalfedgeHandle heh = theMesh.halfedge_handle(fh);
	PolyMesh::VertexHandle vh1 = theMesh.to_vertex_handle(heh);
	PolyMesh::VertexHandle vh2 = theMesh.to_vertex_handle(theMesh.next_halfedge_handle(heh));
	PolyMesh::VertexHandle vh3 =
		theMesh.to_vertex_handle(theMesh.next_halfedge_handle(theMesh.next_halfedge_handle(heh)));

	return getTriaQualityFactor(theMesh, vh1, vh2, vh3);
}













double getTriaQualityFactor(const PolyMesh& theMesh,
	const PolyMesh::VertexHandle& v1,
	const PolyMesh::VertexHandle& v2,
	const PolyMesh::VertexHandle& v3)
{

	double d1 = getDistance(theMesh, v1, v2);
	double d2 = getDistance(theMesh, v2, v3);
	double d3 = getDistance(theMesh, v1, v3);
	double d = (d1 + d2 + d3) / 2.0;

	double area = getArea(theMesh, v1, v2, v3);
	
	return area * 4.0 * 1.7320508 / (d1*d1+d2*d2+d3*d3);
}









double getQuadQualityFactor(const PolyMesh& theMesh,
	const PolyMesh::VertexHandle& v1,
	const PolyMesh::VertexHandle& v2,
	const PolyMesh::VertexHandle& v3,
	const PolyMesh::VertexHandle& v4)
{

	double triaQuality1 = getTriaQualityFactor(theMesh, v1, v2, v3);
	double triaQuality2 = getTriaQualityFactor(theMesh, v1, v2, v4);
	double triaQuality3 = getTriaQualityFactor(theMesh, v3, v4, v2);
	double triaQuality4 = getTriaQualityFactor(theMesh, v3, v4, v1);

	std::vector<double> triaQualities = { triaQuality1 ,triaQuality2, triaQuality3, triaQuality4};
	std::sort(triaQualities.begin(), triaQualities.end(), std::greater<double>());

	return triaQualities[2] * triaQualities[3] / triaQualities[0] / triaQualities[1];
}









bool checkVetexConnection(PolyMesh& theMesh,
	const PolyMesh::VertexHandle& vh1,
	const PolyMesh::VertexHandle& vh2)
{
	// 遍历节点1的相邻节点
	for (PolyMesh::VertexVertexIter vv_it = theMesh.vv_begin(vh1); vv_it != theMesh.vv_end(vh1); ++vv_it)
	{
		PolyMesh::VertexHandle vh = *vv_it;
		if (vh == vh2)
		{
			// 节点2是节点1的相邻节点
			return true;
		}
	}
	return false;
}













bool checkIsolatedTriangle(PolyMesh& theMesh)
{
	// 统计三角形面数量
	int numTriangles = 0;
	for (PolyMesh::FaceIter f_it = theMesh.faces_begin(); f_it != theMesh.faces_end(); ++f_it)
	{
		PolyMesh::FaceHandle fh = *f_it;
		// valence是指一个顶点或一个面周围的边的数量
		if (theMesh.valence(fh) == 3)
		{
			numTriangles++;
		}
	}

	// 如果三角形面不存在，则返回false
	if (numTriangles == 0)
	{
		return false;
	}

	// 统计只与一个三角形面相邻的边缘数量，并与三角形面数量进行比较
	int numEdgesWithOneTriangle = 0;



	// 遍历所有边缘
	for (PolyMesh::EdgeIter e_it = theMesh.edges_begin(); e_it != theMesh.edges_end(); ++e_it)
	{
		PolyMesh::EdgeHandle eh = *e_it;

		// 获取与边缘相邻的两个面
		PolyMesh::HalfedgeHandle heh1 = theMesh.halfedge_handle(eh, 0);
		PolyMesh::HalfedgeHandle heh2 = theMesh.halfedge_handle(eh, 1);
		PolyMesh::FaceHandle fh1 = theMesh.face_handle(heh1);
		PolyMesh::FaceHandle fh2 = theMesh.face_handle(heh2);

		// 如果只有一个三角形面与边缘相邻，则输出边缘编号
		if (fh1.is_valid() && theMesh.valence(fh1) == 3 && (!fh2.is_valid() || (fh2.is_valid() && theMesh.valence(fh2) == 4)))
		{
			numEdgesWithOneTriangle++;
		}
		else if (fh2.is_valid() && theMesh.valence(fh2) == 3 && (!fh1.is_valid() || (fh1.is_valid() && theMesh.valence(fh1) == 4)))
		{
			numEdgesWithOneTriangle++;
		}
	}

	if (numEdgesWithOneTriangle == 3) {
		return false;
	}

	if (numEdgesWithOneTriangle != numTriangles * 3)
	{
		return true;
	}
	else 
	{
		return false;
	}
}


/*
void laplacianPolyMesh(PolyMesh& theMesh, int theVX)
{
	PolyMesh::VertexHandle vh(theVX);

	// 迭代10次进行平滑处理
	for (int i = 0; i < 10; i++)
	{
		// 计算相邻节点的平均位置
		PolyMesh::Vec3f avg(0, 0, 0);
		int count = 0;
		for (PolyMesh::VertexVertexIter vv_it = theMesh.vv_iter(vh); vv_it.is_valid(); ++vv_it)
		{
			avg += theMesh.point(*vv_it);
			count++;
		}
		avg /= count;

		// 将目标节点移向平均位置
		PolyMesh::Vec3f p = theMesh.point(vh);
		p += 0.5 * (avg - p);
		theMesh.set_point(vh, p);
	}
}
*/



// v1-------v2
//  |       |
//  |       |
//  |       |
// v4-------v3
// for v1 in [list v1 v2 v3 v4]  return [list v1 v2 v4 v4]

std::vector<PolyMesh::VertexHandle> sortQuadVertices(const std::vector<PolyMesh::VertexHandle>& theVertices, 
	const PolyMesh::VertexHandle& theVertex) 
{
	// find the index of the node in the list
	int idx = std::find(theVertices.begin(), theVertices.end(), theVertex) - theVertices.begin(); 

	if (idx == 0) {
		return { theVertex, theVertices[1], theVertices[3], theVertices[2] };
	}
	else if (idx == 1) {
		return { theVertex, theVertices[0], theVertices[2], theVertices[3] };
	}
	else if (idx == 2) {
		return { theVertex, theVertices[1], theVertices[3], theVertices[0] };
	}
	else {
		return { theVertex, theVertices[0], theVertices[2], theVertices[1] };
	}
}




std::vector<PolyMesh::VertexHandle> sortQuadVertices(PolyMesh& theMesh,
	PolyMesh::FaceHandle& theFace,
	PolyMesh::VertexHandle& theVertex)
{
	std::vector<PolyMesh::VertexHandle> quadVertices;
	for (PolyMesh::FaceVertexIter fv_it = theMesh.fv_iter(theFace); fv_it.is_valid(); ++fv_it) {
		quadVertices.push_back(*fv_it);
	}

	// find the index of the node in the list
	int idx = std::find(quadVertices.begin(), quadVertices.end(), theVertex) - quadVertices.begin();

	if (idx == 0) {
		return { theVertex, quadVertices[1], quadVertices[3], quadVertices[2] };
	}
	else if (idx == 1) {
		return { theVertex, quadVertices[0], quadVertices[2], quadVertices[3] };
	}
	else if (idx == 2) {
		return { theVertex, quadVertices[1], quadVertices[3], quadVertices[0] };
	}
	else {
		return { theVertex, quadVertices[0], quadVertices[2], quadVertices[1] };
	}
}




bool checkRecoveryFeasibility(PolyMesh& theMesh, 
	const PolyMesh::VertexHandle& vh1,
	const PolyMesh::VertexHandle& vh2) 
{
	std::vector<PolyMesh::FaceHandle> adjQuadFaces;
	for (PolyMesh::VertexFaceIter vf_it = theMesh.vf_iter(vh1); vf_it.is_valid(); ++vf_it) {
		if (theMesh.valence(*vf_it) == 4) {
			adjQuadFaces.push_back(*vf_it);
		}
	}

	if (adjQuadFaces.size() == 0) {
		return true;
	}
	else {
		for (auto qh : adjQuadFaces) {
			std::vector<PolyMesh::VertexHandle> faceVertices;
			for (PolyMesh::FaceVertexIter fv_it = theMesh.fv_iter(qh); fv_it.is_valid(); ++fv_it) {
				faceVertices.push_back(*fv_it);
			}
			if (sortQuadVertices(faceVertices, vh1)[3] == vh2) {
				return false;
			}
		}

	}
	return true;
}








void findTriaEdges(PolyMesh& theMesh, 
	std::vector<PolyMesh::EdgeHandle>& theEdges) 
{
	// 遍历所有边缘
	for (PolyMesh::EdgeIter e_it = theMesh.edges_begin(); e_it != theMesh.edges_end(); ++e_it)
	{
		PolyMesh::EdgeHandle eh = *e_it;

		// 获取与边缘相邻的两个面
		PolyMesh::HalfedgeHandle heh1 = theMesh.halfedge_handle(eh, 0);
		PolyMesh::HalfedgeHandle heh2 = theMesh.halfedge_handle(eh, 1);
		PolyMesh::FaceHandle fh1 = theMesh.face_handle(heh1);
		PolyMesh::FaceHandle fh2 = theMesh.face_handle(heh2);

		// 如果只有一个三角形面与边缘相邻，则输出边缘编号
		if (fh1.is_valid() && theMesh.valence(fh1) == 3 && (!fh2.is_valid() || (fh2.is_valid() && theMesh.valence(fh2) == 4)))
		{
			theEdges.push_back(eh);
		}
		else if (fh2.is_valid() && theMesh.valence(fh2) == 3 && (!fh1.is_valid() || (fh1.is_valid() && theMesh.valence(fh1) == 4)))
		{
			theEdges.push_back(eh);
		}
	}
}



void findFaceEdges(PolyMesh& theMesh,
	std::vector<PolyMesh::EdgeHandle>& theEdges)
{
	// 遍历所有边缘
	for (PolyMesh::EdgeIter e_it = theMesh.edges_begin(); e_it != theMesh.edges_end(); ++e_it)
	{
		PolyMesh::EdgeHandle eh = *e_it;

		// 获取与边缘相邻的两个面
		PolyMesh::HalfedgeHandle heh1 = theMesh.halfedge_handle(eh, 0);
		PolyMesh::HalfedgeHandle heh2 = theMesh.halfedge_handle(eh, 1);
		PolyMesh::FaceHandle fh1 = theMesh.face_handle(heh1);
		PolyMesh::FaceHandle fh2 = theMesh.face_handle(heh2);

		// 如果只有一个三角形面与边缘相邻，则输出边缘编号
		if (fh1.is_valid() && !fh2.is_valid())
		{
			theEdges.push_back(eh);
		}
		else if (fh2.is_valid() && !fh1.is_valid())
		{
			theEdges.push_back(eh);
		}
	}
}










void getEdgeVertices(PolyMesh& theMesh,
	std::vector<PolyMesh::EdgeHandle>& theEdges,
	std::set<PolyMesh::VertexHandle>& theVertices) 
{

	for (auto eh : theEdges) {
		PolyMesh::HalfedgeHandle heh1 = theMesh.halfedge_handle(eh, 0);
		PolyMesh::HalfedgeHandle heh2 = theMesh.halfedge_handle(eh, 1);
		PolyMesh::VertexHandle vh1 = theMesh.to_vertex_handle(heh1);
		PolyMesh::VertexHandle vh2 = theMesh.to_vertex_handle(heh2);
		theVertices.insert(vh1);
		theVertices.insert(vh2);
	}
}


void jacobiLaplaceSmooth(PolyMesh& theMesh)
{
	theMesh.garbage_collection();

	OpenMesh::Smoother::SmootherT< PolyMesh >::Component com =
		OpenMesh::Smoother::SmootherT< PolyMesh >::Tangential;
	OpenMesh::Smoother::SmootherT< PolyMesh >::Continuity con =
		OpenMesh::Smoother::SmootherT< PolyMesh >::C0;

	OpenMesh::Smoother::JacobiLaplaceSmootherT<PolyMesh> smoother(theMesh);
	smoother.initialize(com, con);
	smoother.smooth(5);
	
#ifdef _DEBUG
	saveToVtkFile(theMesh, "jacobiLaplace_after.vtk");
	std::cout << "jacobiLaplaceSmooth " << "---- finish" << std::endl;
#endif // _DEBUG
}





void equivalencePolyMesh(PolyMesh& theMesh,
								double    theTol) 
{

	theMesh.request_vertex_status();
	theMesh.request_edge_status();
	theMesh.request_face_status();


	// 迭代遍历所有的边
	for (PolyMesh::EdgeIter e_it = theMesh.edges_begin(); e_it != theMesh.edges_end(); ++e_it)
	{
		PolyMesh::EdgeHandle eh = *e_it;

		// 获取边的两个顶点句柄
		PolyMesh::HalfedgeHandle heh1 = theMesh.halfedge_handle(eh, 0);
		PolyMesh::HalfedgeHandle heh2 = theMesh.halfedge_handle(eh, 1);
		PolyMesh::VertexHandle vh1 = theMesh.to_vertex_handle(heh1);
		PolyMesh::VertexHandle vh2 = theMesh.to_vertex_handle(heh2);

		// 计算顶点距离
		double distance = (theMesh.point(vh1) - theMesh.point(vh2)).norm();

		// 如果顶点距离小于阈值，则缩短边
		if (distance < theTol)
		{
			theMesh.collapse(heh2);
		}
	}
	theMesh.garbage_collection();

}










bool checkVertexTranslation(PolyMesh& theMesh,
	PolyMesh::VertexHandle& vh)
{
	for (PolyMesh::VertexFaceIter vf_it = theMesh.vf_iter(vh); vf_it.is_valid(); ++vf_it) {
		if (theMesh.valence(*vf_it) == 4) {
			return false;
		}
	}
	for (PolyMesh::VertexEdgeIter ve_it = theMesh.ve_iter(vh); ve_it.is_valid(); ++ve_it) {
		PolyMesh::EdgeHandle eh = *ve_it;
		PolyMesh::HalfedgeHandle heh1 = theMesh.halfedge_handle(eh, 0);
		PolyMesh::HalfedgeHandle heh2 = theMesh.halfedge_handle(eh, 1);
		if (!theMesh.face_handle(heh1).is_valid()) {
			return false;
		}
		if (!theMesh.face_handle(heh2).is_valid()) {
			return false;
		}
	}
	return true;
}









bool edgeRecoverySequence(PolyMesh& theMesh, 
	const PolyMesh::VertexHandle& vh1,
	const PolyMesh::VertexHandle& vh2)
{
	if (checkVetexConnection(theMesh, vh1, vh2)) { return true; }
	PolyMesh::VertexHandle vhx;

	while (vhx != vh2) {


		std::vector< PolyMesh::FaceHandle> adjTriaFaces;

		for (PolyMesh::VertexFaceIter vf_it = theMesh.vf_iter(vh1); vf_it.is_valid(); ++vf_it) {
			if (theMesh.valence(*vf_it) == 3) {
				adjTriaFaces.push_back(*vf_it);
			}
		}

		for (auto th : adjTriaFaces) {
			std::vector<PolyMesh::VertexHandle> fvs;
			for (PolyMesh::FaceVertexIter fv_it = theMesh.fv_iter(th); fv_it.is_valid(); ++fv_it) {
				fvs.push_back(*fv_it);
			}
			fvs.erase(std::remove(fvs.begin(), fvs.end(), vh1), fvs.end());

			if (checkFaceAngle(theMesh, vh1, vh2, fvs[0], fvs[1]) == -1.0 &&
				checkFaceAngle(theMesh, fvs[0], fvs[1], vh1, vh2) > 90.0)
			{
				double ang1 = getAngle(theMesh, vh1, fvs[0], vh2);
				double ang2 = getAngle(theMesh, vh1, fvs[1], vh2);
				bool bool1 = checkVertexTranslation(theMesh, fvs[0]);
				bool bool2 = checkVertexTranslation(theMesh, fvs[1]);

				if (!checkVertexTranslation(theMesh, fvs[0]) &&
					!checkVertexTranslation(theMesh, fvs[1])) {
					return false;
				}
				// 轻微扰动
				if (getAngle(theMesh, vh1, fvs[0], vh2) > getAngle(theMesh, vh1, fvs[1], vh2) && checkVertexTranslation(theMesh, fvs[0])) {
					// 获取顶点的坐标
					PolyMesh::Point& p0 = theMesh.point(fvs[0]);
					PolyMesh::Point& p1 = theMesh.point(fvs[1]);
					PolyMesh::Point vector;
					if (getFaceAngle(theMesh, vh1, vh2, fvs[0], fvs[1]) > 90.0) {
						vector = p0 - p1;
					}
					else {
						vector = p1 - p0;
					}
					double distance = vector.norm();
					double move_distance = 0.2 * distance;
					PolyMesh::Point move_vector = vector.normalized() * move_distance;
					p0 += move_vector;
				}
				else if (getAngle(theMesh, vh1, fvs[0], vh2) < getAngle(theMesh, vh1, fvs[1], vh2) && checkVertexTranslation(theMesh, fvs[1])) {

					// 获取顶点的坐标
					PolyMesh::Point& p0 = theMesh.point(fvs[0]);
					PolyMesh::Point& p1 = theMesh.point(fvs[1]);
					PolyMesh::Point vector;
					if (getFaceAngle(theMesh, vh1, vh2, fvs[0], fvs[1]) > 90.0) {
						vector = p1 - p0;
					}
					else {
						vector = p0 - p1;
					}
					double distance = vector.norm();
					double move_distance = 0.2 * distance;
					PolyMesh::Point move_vector = vector.normalized() * move_distance;
					p1 += move_vector;
				}

#ifdef _DEBUG
				// saveToVtkFile(theMesh, "translate_after.vtk");
#endif // _DEBUG
			}
			if (getFaceAngle(theMesh, vh1, vh2, fvs[0], fvs[1]) > 90.0 &&
				getFaceAngle(theMesh, fvs[0], fvs[1], vh1, vh2) > 90.0) {

				if (!checkVertexTranslation(theMesh, fvs[0]) &&
					!checkVertexTranslation(theMesh, fvs[1])) {
					return false;
				}

				std::vector< PolyMesh::FaceHandle> adjFaces_temp;
				std::vector< PolyMesh::FaceHandle> adjFaces_new;
				for (PolyMesh::VertexFaceIter vf_it = theMesh.vf_iter(fvs[0]); vf_it.is_valid(); ++vf_it) {
					adjFaces_temp.push_back(*vf_it);
				}
				for (auto fh : adjFaces_temp) {
					if (std::find(adjTriaFaces.begin(), adjTriaFaces.end(), fh) == adjTriaFaces.end()) {
						adjFaces_new.push_back(fh);
					}
				}
				for (auto fh : adjFaces_new) {
					std::vector<PolyMesh::VertexHandle> fvs_new;
					for (PolyMesh::FaceVertexIter fv_it = theMesh.fv_iter(fh); fv_it.is_valid(); ++fv_it) {
						fvs_new.push_back(*fv_it);
					}
					if (std::find(fvs_new.begin(), fvs_new.end(), fvs[0]) != fvs_new.end() &&
						std::find(fvs_new.begin(), fvs_new.end(), fvs[1]) != fvs_new.end()) {

						if (theMesh.valence(fh) == 4) {
							return 0;
						}
						fvs_new.erase(std::remove(fvs_new.begin(), fvs_new.end(), fvs[0]), fvs_new.end());
						fvs_new.erase(std::remove(fvs_new.begin(), fvs_new.end(), fvs[1]), fvs_new.end());
						vhx = fvs_new[0];

						if (getFaceAngle(theMesh, vh1, vhx, fvs[0], fvs[1]) > 90.0 &&
							getFaceAngle(theMesh, fvs[0], fvs[1], vh1, vhx) > 90.0) {

							theMesh.request_vertex_status();
							theMesh.request_edge_status();
							theMesh.request_face_status();


							theMesh.delete_face(th, false);
							theMesh.delete_face(fh, false);
							theMesh.garbage_collection();

							autoAddFace(theMesh, fvs_new[0], fvs[0], vh1);
							autoAddFace(theMesh, fvs_new[0], fvs[1], vh1);
							
							break;
						}
						else {
							return edgeRecoveryReverse(theMesh, vh2, vh1);
						}
					}
				}
			}
		}
	}
	return true;
}



bool edgeRecoveryReverse(PolyMesh& theMesh, 
	const PolyMesh::VertexHandle& vh1,
	const PolyMesh::VertexHandle& vh2) 
{

	PolyMesh::VertexHandle vhx;

	while (vhx != vh2) {

		std::vector< PolyMesh::FaceHandle> adjTriaFaces;

		for (PolyMesh::VertexFaceIter vf_it = theMesh.vf_iter(vh1); vf_it.is_valid(); ++vf_it) {
			if (theMesh.valence(*vf_it) == 3) {
				adjTriaFaces.push_back(*vf_it);
			}
		}

		for (auto th : adjTriaFaces) {
			std::vector<PolyMesh::VertexHandle> fvs;
			for (PolyMesh::FaceVertexIter fv_it = theMesh.fv_iter(th); fv_it.is_valid(); ++fv_it) {
				fvs.push_back(*fv_it);
			}
			fvs.erase(std::remove(fvs.begin(), fvs.end(), vh1), fvs.end());

			if (checkFaceAngle(theMesh, vh1, vh2, fvs[0], fvs[1]) == -1.0 &&
				checkFaceAngle(theMesh, fvs[0], fvs[1], vh1, vh2) > 90.0)
			{
				if (!checkVertexTranslation(theMesh, fvs[0]) &&
					!checkVertexTranslation(theMesh, fvs[1])) {
					return false;
				}
				// 轻微扰动
				if (getAngle(theMesh, vh1, fvs[0], vh2) > getAngle(theMesh, vh1, fvs[1], vh2) && checkVertexTranslation(theMesh, fvs[0])) {
					// 获取顶点的坐标
					PolyMesh::Point& p0 = theMesh.point(fvs[0]);
					PolyMesh::Point& p1 = theMesh.point(fvs[1]);
					PolyMesh::Point vector;
					if (getFaceAngle(theMesh, vh1, vh2, fvs[0], fvs[1]) > 90.0) {
						vector = p0 - p1;
					}
					else {
						vector = p1 - p0;
					}
					double distance = vector.norm();
					double move_distance = 0.2 * distance;
					PolyMesh::Point move_vector = vector.normalized() * move_distance;
					p0 += move_vector;
				}
				else if (getAngle(theMesh, vh1, fvs[0], vh2) < getAngle(theMesh, vh1, fvs[1], vh2) && checkVertexTranslation(theMesh, fvs[1])) {

					// 获取顶点的坐标
					PolyMesh::Point& p0 = theMesh.point(fvs[0]);
					PolyMesh::Point& p1 = theMesh.point(fvs[1]);
					PolyMesh::Point vector;
					if (getFaceAngle(theMesh, vh1, vh2, fvs[0], fvs[1]) > 90.0) {
						vector = p1 - p0;
					}
					else {
						vector = p0 - p1;
					}
					double distance = vector.norm();
					double move_distance = 0.2 * distance;
					PolyMesh::Point move_vector = vector.normalized() * move_distance;
					p1 += move_vector;
				}

			}
			if (getFaceAngle(theMesh, vh1, vh2, fvs[0], fvs[1]) > 90.0 &&
				getFaceAngle(theMesh, fvs[0], fvs[1], vh1, vh2) > 90.0) {

				if (!checkVertexTranslation(theMesh, fvs[0]) &&
					!checkVertexTranslation(theMesh, fvs[1])) {
					return false;
				}

				std::vector< PolyMesh::FaceHandle> adjFaces_temp;
				std::vector< PolyMesh::FaceHandle> adjFaces_new;
				for (PolyMesh::VertexFaceIter vf_it = theMesh.vf_iter(fvs[0]); vf_it.is_valid(); ++vf_it) {
					adjFaces_temp.push_back(*vf_it);
				}
				for (auto fh : adjFaces_temp) {
					if (std::find(adjTriaFaces.begin(), adjTriaFaces.end(), fh) == adjTriaFaces.end()) {
						adjFaces_new.push_back(fh);
					}
				}
				for (auto fh : adjFaces_new) {
					std::vector<PolyMesh::VertexHandle> fvs_new;
					for (PolyMesh::FaceVertexIter fv_it = theMesh.fv_iter(fh); fv_it.is_valid(); ++fv_it) {
						fvs_new.push_back(*fv_it);
					}
					if (std::find(fvs_new.begin(), fvs_new.end(), fvs[0]) != fvs_new.end() &&
						std::find(fvs_new.begin(), fvs_new.end(), fvs[1]) != fvs_new.end()) {

						if (theMesh.valence(fh) == 4) {
							return 0;
						}
						fvs_new.erase(std::remove(fvs_new.begin(), fvs_new.end(), fvs[0]), fvs_new.end());
						fvs_new.erase(std::remove(fvs_new.begin(), fvs_new.end(), fvs[1]), fvs_new.end());
						vhx = fvs_new[0];

						theMesh.request_vertex_status();
						theMesh.request_edge_status();
						theMesh.request_face_status();

						theMesh.delete_face(th, false);
						theMesh.delete_face(fh, false);
						theMesh.garbage_collection();
						
						autoAddFace(theMesh, fvs_new[0], fvs[0], vh1);
						autoAddFace(theMesh, fvs_new[0], fvs[1], vh1);

						break;
					}
				}
			}
		}
	}
	return true;
}






std::vector<PolyMesh::EdgeHandle> getGroupBoundaryEdges(PolyMesh& theMesh, 
												std::vector<PolyMesh::EdgeHandle>& theEdges) 
{

	std::vector<std::vector<PolyMesh::EdgeHandle>> theGroupBoundaryEdges;
	std::vector<PolyMesh::EdgeHandle> boundaryEdges = theEdges;
	
#ifdef _DEBUG
	std::cout << "------------------------------------" << std::endl;
	std::cout << "number of edges: " << boundaryEdges.size() << std::endl;
	std::cout << "------------------------------------" << std::endl;
#endif // _DEBUG

	std::vector<PolyMesh::EdgeHandle> processedEdges;

	while (!boundaryEdges.empty()) {

		std::vector<PolyMesh::EdgeHandle> group;

		// 从未处理的边缘中任选一条作为起始边缘
		PolyMesh::EdgeHandle start_edge = boundaryEdges.back();

		boundaryEdges.pop_back();

		// 将起始边缘加入当前分组
		group.push_back(start_edge);
		processedEdges.push_back(start_edge);
		// 循环查找相邻的边缘，直到找到的四条边缘都已经处理过
		while (true) {

			PolyMesh::HalfedgeHandle heh1_new = theMesh.halfedge_handle(start_edge, 0);
			PolyMesh::HalfedgeHandle heh2_new = theMesh.halfedge_handle(start_edge, 1);
			PolyMesh::VertexHandle vh1_new = theMesh.to_vertex_handle(heh1_new);
			PolyMesh::VertexHandle vh2_new = theMesh.to_vertex_handle(heh2_new);

			std::set<PolyMesh::EdgeHandle> edgeSet;
			for (PolyMesh::VertexEdgeIter ve_it = theMesh.ve_iter(vh1_new); ve_it.is_valid(); ++ve_it) {
				if (std::find(boundaryEdges.begin(), boundaryEdges.end(), *ve_it) != boundaryEdges.end()) {
					edgeSet.insert(*ve_it);
				}
			}
			for (PolyMesh::VertexEdgeIter ve_it = theMesh.ve_iter(vh2_new); ve_it.is_valid(); ++ve_it) {
				if (std::find(boundaryEdges.begin(), boundaryEdges.end(), *ve_it) != boundaryEdges.end()) {
					edgeSet.insert(*ve_it);
				}
			}
			std::vector<PolyMesh::EdgeHandle> edgeVector;
			for (auto eh : edgeSet) {
				if (std::find(processedEdges.begin(), processedEdges.end(), eh) == processedEdges.end()) {
					edgeVector.push_back(eh);
				}
			}
			if (edgeVector.size() == 0) {
				break;
			}
			else {
				start_edge = edgeVector[0];
				group.push_back(start_edge);
				boundaryEdges.erase(std::remove(boundaryEdges.begin(), boundaryEdges.end(), start_edge), 
					boundaryEdges.end());
				processedEdges.push_back(start_edge);
			}
			
		}

		// 将当前分组加入分组vector中
		theGroupBoundaryEdges.push_back(group);
	}

#ifdef _DEBUG
	std::cout << "------------------------------------" << std::endl;
	std::cout << "number of groups: " << theGroupBoundaryEdges.size() << std::endl;
	std::cout << "------------------------------------" << std::endl;

	for (auto group : theGroupBoundaryEdges) {
		std::cout << "------------------------------------" << std::endl;
		std::cout << "number of group edges: " << group.size() << std::endl;
		std::cout << "------------------------------------" << std::endl;
	}
#endif // _DEBUG

	// 计算每个分组的平均边缘长度
	std::vector<double> group_mean_lengths;
	for (std::vector<PolyMesh::EdgeHandle> group : theGroupBoundaryEdges) {
		double group_length = 0.0;
		for (PolyMesh::EdgeHandle edge : group) {
			group_length += theMesh.calc_edge_length(edge);
		}
		double mean_length = group_length / (double)group.size();
		group_mean_lengths.push_back(mean_length);
	}

	// 找到平均边缘长度最小的分组
	int min_index = 0;
	for (int i = 1; i < group_mean_lengths.size(); i++) {
		if (group_mean_lengths[i] < group_mean_lengths[min_index]) {
			min_index = i;
		}
	}
	
#ifdef _DEBUG
	// 输出平均边缘长度最小的分组
	std::cout << "Minimum mean edge length group: ";
	for (PolyMesh::EdgeHandle edge : theGroupBoundaryEdges[min_index]) {
		std::cout << edge.idx() << " ";
	}
	std::cout << std::endl;
#endif // _DEBUG

	return theGroupBoundaryEdges[min_index];
}



PolyMesh::VertexHandle getThirdVertex(PolyMesh& theMesh, 
	const PolyMesh::VertexHandle& vh1,
	const PolyMesh::VertexHandle& vh2,
	const std::vector<PolyMesh::EdgeHandle>& theEdges) 
{
	PolyMesh::VertexHandle vh;

	std::set< PolyMesh::VertexHandle> vertices;
	
	for (auto theEdge: theEdges) {
		if (theMesh.is_valid_handle(theEdge)) {
			PolyMesh::HalfedgeHandle heh1 = theMesh.halfedge_handle(theEdge, 0);
			PolyMesh::HalfedgeHandle heh2 = theMesh.halfedge_handle(theEdge, 1);
			PolyMesh::VertexHandle theVh1 = theMesh.to_vertex_handle(heh1);
			PolyMesh::VertexHandle theVh2 = theMesh.to_vertex_handle(heh2);

			if (theVh1 == vh1) {
				if (theVh2 != vh2) {
					return theVh2;
				}
			}
			else if (theVh2 == vh1) {
				if (theVh1 != vh2) {
					return theVh1;
				}
			}
		}
		
	}
	
	return vh;
}




std::vector<std::pair<PolyMesh::VertexHandle, double>> getVertexAngleList(PolyMesh& theMesh,
	const PolyMesh::VertexHandle& currentVertex, 
	const PolyMesh::VertexHandle& secondVertex,
	const PolyMesh::VertexHandle& thirdVertex) 
{

	PolyMesh::Point tempVertexpoint;
	
	double total_angle = 0;
	std::vector<std::pair<PolyMesh::VertexHandle, double>> vertexAngleList;
	vertexAngleList.push_back(std::make_pair(secondVertex, total_angle));

	PolyMesh::VertexHandle tempVertex = secondVertex;
	std::vector<PolyMesh::FaceHandle> processedFaces;
	while (tempVertex != thirdVertex) {
		std::vector<PolyMesh::FaceHandle> adjFaces;
		
		for (PolyMesh::VertexFaceIter vf_it = theMesh.vf_iter(currentVertex); vf_it.is_valid(); ++vf_it) {
			if (tempVertex == secondVertex) {
				if (theMesh.valence(*vf_it) == 3) {
					adjFaces.push_back(*vf_it);
				}
			}
			else {
				adjFaces.push_back(*vf_it);
			}
		}
		for (auto adjfh : adjFaces) {
			std::vector<PolyMesh::VertexHandle> faceVertices;
			for (PolyMesh::FaceVertexIter fv_it = theMesh.cfv_iter(adjfh); fv_it.is_valid(); ++fv_it) {
				faceVertices.push_back(*fv_it);
			}
			if (std::find(faceVertices.begin(), faceVertices.end(), currentVertex) != faceVertices.end() &&
				std::find(faceVertices.begin(), faceVertices.end(), tempVertex) != faceVertices.end() &&
				std::find(processedFaces.begin(), processedFaces.end(), adjfh) == processedFaces.end()) {

				std::vector<PolyMesh::VertexHandle> sortVertices;
				if (theMesh.valence(adjfh) == 4) {
					std::vector<PolyMesh::VertexHandle> tempSortVertices = sortQuadVertices(faceVertices, currentVertex);
					sortVertices.push_back(tempSortVertices[0]);
					sortVertices.push_back(tempSortVertices[1]);
					sortVertices.push_back(tempSortVertices[2]);
				}
				else {
					sortVertices = faceVertices;
				}
				sortVertices.erase(std::remove(sortVertices.begin(), sortVertices.end(), currentVertex), 
					sortVertices.end());
				sortVertices.erase(std::remove(sortVertices.begin(), sortVertices.end(), tempVertex), sortVertices.end());
				PolyMesh::VertexHandle objectVertex = sortVertices[0];

				total_angle = total_angle + getAngle(theMesh, tempVertex, currentVertex, objectVertex);
				vertexAngleList.push_back(std::make_pair(objectVertex, total_angle));

				tempVertex = objectVertex;
				processedFaces.push_back(adjfh);

				break;
			}
		}
	}

	return vertexAngleList;
}




bool checkTriaByVertex(PolyMesh& mesh, 
	const PolyMesh::VertexHandle& v1, 
	const PolyMesh::VertexHandle& v2, 
	const PolyMesh::VertexHandle& v3)
{
	for (PolyMesh::FaceIter f_it = mesh.faces_begin(); f_it != mesh.faces_end(); ++f_it) {
		int count = 0;
		for (PolyMesh::FaceVertexIter fv_it = mesh.fv_iter(*f_it); fv_it.is_valid(); ++fv_it) {
			if (*fv_it == v1 || *fv_it == v2 || *fv_it == v3) {
				count++;
			}
		}
		if (count == 3) {
			return true;
		}
	}
	return false;
}



PolyMesh::FaceHandle getTriaByVertex(PolyMesh& theMesh, 
	const PolyMesh::VertexHandle& node1,
	const PolyMesh::VertexHandle& node2,
	const PolyMesh::VertexHandle& node3)
{
	for (PolyMesh::VertexFaceIter vf_it = theMesh.vf_iter(node1); vf_it.is_valid(); ++vf_it) {
		PolyMesh::FaceHandle face_handle = *vf_it;
		std::set<PolyMesh::VertexHandle> node_set;
		PolyMesh::FaceVertexIter fv_it = theMesh.fv_iter(face_handle);
		PolyMesh::VertexHandle v0 = *fv_it; ++fv_it;
		PolyMesh::VertexHandle v1 = *fv_it; ++fv_it;
		PolyMesh::VertexHandle v2 = *fv_it;
		node_set.insert(v0);
		node_set.insert(v1);
		node_set.insert(v2);
		if (node_set.find(node2) != node_set.end() && node_set.find(node3) != node_set.end()) {
			return face_handle;
		}
	}
	return PolyMesh::InvalidFaceHandle;
}





void deleteTriaInQuad(PolyMesh& theMesh,
	const PolyMesh::VertexHandle& vh1,
	const PolyMesh::VertexHandle& vh2,
	const PolyMesh::VertexHandle& vh3,
	const PolyMesh::VertexHandle& vh4)
{
	// 
	std::vector< PolyMesh::VertexHandle > quadVhs = { vh1, vh2, vh3, vh4 };
	std::set< PolyMesh::FaceHandle> adjTHS;
	std::set< PolyMesh::FaceHandle> twoTHS;

	for (auto vh : quadVhs) {
		for (PolyMesh::VertexFaceIter vf_it = theMesh.vf_iter(vh); vf_it.is_valid(); ++vf_it) {
			if (theMesh.valence(*vf_it) == 3) {
				std::vector< PolyMesh::VertexHandle> vhs;
				for (PolyMesh::FaceVertexIter fv_it = theMesh.fv_iter(*vf_it); fv_it.is_valid(); ++fv_it) {
					vhs.push_back(*fv_it);
				}
				if (std::find(quadVhs.begin(), quadVhs.end(), vhs[0]) != quadVhs.end() &&
					std::find(quadVhs.begin(), quadVhs.end(), vhs[1]) != quadVhs.end() &&
					std::find(quadVhs.begin(), quadVhs.end(), vhs[2]) != quadVhs.end()) {
					twoTHS.insert(*vf_it);
				}
				else {
					adjTHS.insert(*vf_it);
				}
			}
		}
	}

	if (twoTHS.size() == 2) {
		// 如果四边形由两个三角形构成
		theMesh.request_vertex_status();
		theMesh.request_edge_status();
		theMesh.request_face_status();
		for (auto th : twoTHS) {
			theMesh.delete_face(th, false);
		}
		theMesh.garbage_collection(false);
	}
	else {
		theMesh.request_vertex_status();
		theMesh.request_edge_status();
		theMesh.request_face_status();
		for (auto th : twoTHS) {
			theMesh.delete_face(th, false);
		}

		OpenMesh::Vec3f quadCenter(0, 0, 0);
		for (int i = 0; i < 4; i++) {
			quadCenter += theMesh.point(quadVhs[i]);
		}
		quadCenter /= 4;

		// 在中心点处创建一个新的节点
		PolyMesh::VertexHandle qvh = theMesh.add_vertex(quadCenter);

		std::vector<std::vector<PolyMesh::VertexHandle>> quadVhVh =
		{ {vh1, vh2}, {vh2, vh3} , {vh3, vh4} ,{vh4, vh1} };

		for (auto th : adjTHS) {
			std::set< PolyMesh::VertexHandle > vhs = { vh1, vh2, vh3, vh4 };
			for (PolyMesh::FaceVertexIter fv_it = theMesh.fv_iter(th); fv_it.is_valid(); ++fv_it) {
				vhs.insert(*fv_it);
			}
			PolyMesh::VertexHandle vh;
			if (vhs.size() == 5) {
				vhs.erase(vh1);
				vhs.erase(vh2);
				vhs.erase(vh3);
				vhs.erase(vh4);
				vh = *vhs.begin();
			}
			else {
				OpenMesh::Vec3f center(0, 0, 0);
				for (PolyMesh::FaceVertexIter fv_it = theMesh.fv_iter(th); fv_it.is_valid(); ++fv_it) {
					center += theMesh.point(*fv_it);
				}
				center /= 3;
				// 在中心点处创建一个新的节点
				vh = theMesh.add_vertex(center);
			}

			int deleteFlag = 0;
			for (auto vhvh : quadVhVh) {
				double ang = getFaceAngle(theMesh, vhvh[0], vhvh[1], qvh, vh);
				if (ang > 90.0) {
					deleteFlag = 1;
					break;
				}
			}

			if (deleteFlag == 0) {
				theMesh.delete_face(th, false);
			}
		}
		theMesh.garbage_collection(false);
	}
#ifdef _DEBUG
	saveToVtkFile(theMesh, "delete_after.vtk");
#endif // _DEBUG
}





void mergeBadTriaByQuad(PolyMesh& theMesh, 
	PolyMesh::FaceHandle& theQuad, 
	std::set< PolyMesh::VertexHandle > theVertices) 
{

	if (!theMesh.is_valid_handle(theQuad)) {
		return;
	}

	theMesh.request_vertex_status();
	theMesh.request_edge_status();
	theMesh.request_face_status();



	std::vector< PolyMesh::VertexHandle > quadVertices;
	for (PolyMesh::FaceVertexIter fv_it = theMesh.fv_iter(theQuad); fv_it.is_valid(); ++fv_it) {
		quadVertices.push_back(*fv_it);
	}

	std::set< PolyMesh::FaceHandle> triaSet;
	for (auto vh : quadVertices) {
		for (PolyMesh::VertexFaceIter vf_it = theMesh.vf_iter(vh); vf_it.is_valid(); ++vf_it) {
			if (theMesh.valence(*vf_it) == 3) {
				triaSet.insert(*vf_it);
			}
		}
	}
	
	for (auto th : triaSet) {
		std::set< PolyMesh::VertexHandle > tempVertices;
		for (PolyMesh::FaceVertexIter fv_it = theMesh.fv_iter(theQuad); fv_it.is_valid(); ++fv_it) {
			tempVertices.insert(*fv_it);
		}
		for (PolyMesh::FaceVertexIter fv_it = theMesh.fv_iter(th); fv_it.is_valid(); ++fv_it) {
			tempVertices.insert(*fv_it);
		}

		if (tempVertices.size() == 5 && getTriaQualityFactor(theMesh, th) <= 0.54) {
			tempVertices.erase(quadVertices[0]);
			tempVertices.erase(quadVertices[1]);
			tempVertices.erase(quadVertices[2]);
			tempVertices.erase(quadVertices[3]);

			PolyMesh::VertexHandle reflectVH = *tempVertices.begin();

			std::set< PolyMesh::FaceHandle> tempQuadSet;
			for (PolyMesh::VertexFaceIter vf_it = theMesh.vf_iter(reflectVH); vf_it.is_valid(); ++vf_it) {
				if (theMesh.valence(*vf_it) == 4) {
					tempQuadSet.insert(*vf_it);
				}
			}
			if (tempQuadSet.size() == 0 &&
				std::find(theVertices.begin(), theVertices.end(), reflectVH) == theVertices.end()) {
				std::vector<PolyMesh::VertexHandle> tempTriaVertices;
				for (PolyMesh::FaceVertexIter fv_it = theMesh.fv_iter(th); fv_it.is_valid(); ++fv_it) {
					tempTriaVertices.push_back(*fv_it);
				}

				tempTriaVertices.erase(std::remove(tempTriaVertices.begin(), tempTriaVertices.end(), reflectVH), 
					tempTriaVertices.end());
				
				theMesh.request_vertex_status();
				theMesh.request_edge_status();
				theMesh.request_face_status();
				if (getDistance(theMesh, tempTriaVertices[0], reflectVH) >= 
					getDistance(theMesh, tempTriaVertices[1], reflectVH)) 
				{
					
					PolyMesh::HalfedgeHandle halfEdgeHandle = theMesh.find_halfedge(tempTriaVertices[1], reflectVH);
					if (theMesh.is_valid_handle(halfEdgeHandle)) {
						theMesh.collapse(halfEdgeHandle);
					}
				}
				else {
					PolyMesh::HalfedgeHandle halfEdgeHandle = theMesh.find_halfedge(tempTriaVertices[0], reflectVH);
					if (theMesh.is_valid_handle(halfEdgeHandle)) {
						theMesh.collapse(halfEdgeHandle);
					}
				}
				theMesh.garbage_collection();
				break;
			}
		}
	}
}





// v1----------v2                 v1----------v2
//  |    /\    |									 |     |     |
//  |   /  \   |									 |     |     |
//  |---    ---|      >>>>>>>      |-----|-----| 
//  |   \  /   |									 |     |     |
//  |    \/    |									 |     |     |
// v4----------v3									v4----------v3


void closeDiamondQuad(PolyMesh& theMesh,
	vtkSmartPointer<vtkPoints>&   thePoints,
	vtkSmartPointer<vtkPolyData>& thePolyData,
	vtkSmartPointer<vtkRenderWindow>& theRender)
{
	int closeFlag = 1;
	while (closeFlag) {
		closeFlag = 0;


		std::vector<PolyMesh::EdgeHandle> faceEdges;
		findFaceEdges(theMesh, faceEdges);

		std::set<PolyMesh::VertexHandle> boundaryVertices;
		getEdgeVertices(theMesh, faceEdges, boundaryVertices);

		for (PolyMesh::FaceIter f_it = theMesh.faces_begin(); f_it != theMesh.faces_end(); ++f_it) {
			PolyMesh::FaceHandle fh = *f_it;
			if (theMesh.valence(fh) == 4)
			{
				int bounaryFlag = 0;
				for (PolyMesh::FaceEdgeIter fe_it = theMesh.fe_iter(fh); fe_it.is_valid(); ++fe_it) {
					PolyMesh::EdgeHandle eh = *fe_it;

					PolyMesh::HalfedgeHandle heh1 = theMesh.halfedge_handle(eh, 0);
					PolyMesh::HalfedgeHandle heh2 = theMesh.halfedge_handle(eh, 1);
					PolyMesh::FaceHandle fh1 = theMesh.face_handle(heh1);
					PolyMesh::FaceHandle fh2 = theMesh.face_handle(heh2);

					if (!fh1.is_valid() || !fh2.is_valid()) {
						bounaryFlag = 1;
						break;
					}
				}
				if (bounaryFlag == 1) {
					continue;
				}
				std::vector< PolyMesh::VertexHandle > quadVertices;
				for (PolyMesh::FaceVertexIter fv_it = theMesh.fv_iter(fh); fv_it.is_valid(); ++fv_it) {
					quadVertices.push_back(*fv_it);
				}
				if (theMesh.valence(quadVertices[0]) == 3 && 
					theMesh.valence(quadVertices[2]) == 3 &&
					std::find(boundaryVertices.begin(), boundaryVertices.end(), quadVertices[0]) == boundaryVertices.end() &&
					std::find(boundaryVertices.begin(), boundaryVertices.end(), quadVertices[2]) == boundaryVertices.end()) 
				{
#ifdef _DEBUG
					if (theMesh.is_valid_handle(quadVertices[0])) {
						PolyMesh::Point vh1_point = theMesh.point(quadVertices[0]);
						std::cout << "vh1_point : " << vh1_point << std::endl;
					}
					if (theMesh.is_valid_handle(quadVertices[2])) {
						PolyMesh::Point vh2_point = theMesh.point(quadVertices[2]);
						std::cout << "vh2_point : " << vh2_point << std::endl;
					}
#endif // _DEBUG

					theMesh.request_vertex_status();
					theMesh.request_edge_status();
					theMesh.request_face_status();

					theMesh.delete_face(fh, false);
					theMesh.garbage_collection(false);

					autoAddFace(theMesh, quadVertices[0], quadVertices[2], quadVertices[1]);
					autoAddFace(theMesh, quadVertices[0], quadVertices[2], quadVertices[3]);

					PolyMesh::Point& p0 = theMesh.point(quadVertices[0]);
					PolyMesh::Point& p1 = theMesh.point(quadVertices[2]);
					PolyMesh::Point vector = p0 - p1;
					double distance = vector.norm();
					double move_distance = 0.5 * distance;
					PolyMesh::Point move_vector = vector.normalized() * move_distance;
					p1 += move_vector;

					PolyMesh::HalfedgeHandle halfEdgeHandle = theMesh.find_halfedge(quadVertices[0], quadVertices[2]);
					if (theMesh.is_valid_handle(halfEdgeHandle)) {
						theMesh.collapse(halfEdgeHandle);
						
					}
#ifdef _DEBUG
					saveToVtkFile(theMesh, "close_after.vtk");
#endif // _DEBUG
					closeFlag = 1;
					break;
				}
				else if (theMesh.valence(quadVertices[1]) == 3 && theMesh.valence(quadVertices[3]) == 3 && std::find(boundaryVertices.begin(), boundaryVertices.end(), quadVertices[1]) == boundaryVertices.end() && std::find(boundaryVertices.begin(), boundaryVertices.end(), quadVertices[3]) == boundaryVertices.end())
				{
#ifdef _DEBUG
					if (theMesh.is_valid_handle(quadVertices[1])) {
						PolyMesh::Point vh1_point = theMesh.point(quadVertices[1]);
						std::cout << "vh1_point : " << vh1_point << std::endl;
					}
					if (theMesh.is_valid_handle(quadVertices[3])) {
						PolyMesh::Point vh2_point = theMesh.point(quadVertices[3]);
						std::cout << "vh2_point : " << vh2_point << std::endl;
					}
#endif // _DEBUG

					theMesh.request_vertex_status();
					theMesh.request_edge_status();
					theMesh.request_face_status();

					theMesh.delete_face(fh, false);
					theMesh.garbage_collection(false);

					autoAddFace(theMesh, quadVertices[1], quadVertices[3], quadVertices[0]);
					autoAddFace(theMesh, quadVertices[1], quadVertices[3], quadVertices[2]);

					PolyMesh::Point& p0 = theMesh.point(quadVertices[1]);
					PolyMesh::Point& p1 = theMesh.point(quadVertices[3]);
					PolyMesh::Point vector = p0 - p1;
					double distance = vector.norm();
					double move_distance = 0.5 * distance;
					PolyMesh::Point move_vector = vector.normalized() * move_distance;
					p1 += move_vector;

					PolyMesh::HalfedgeHandle halfEdgeHandle = theMesh.find_halfedge(quadVertices[1], quadVertices[3]);
					if (theMesh.is_valid_handle(halfEdgeHandle)) {
						theMesh.collapse(halfEdgeHandle);
					}
#ifdef _DEBUG
					saveToVtkFile(theMesh, "close_after.vtk");
#endif // _DEBUG

					closeFlag = 1;
					break;
				}
			}
		}
	}
	// jacobiLaplaceSmooth(theMesh);
#ifdef _DEBUG
	updateVtkView(thePoints, thePolyData, theRender, theMesh);
#endif // _DEBUG
}



//    -----------                       -----------
//   /\        /\											  / \        \
//  /  \      /  \										 /    \       \
//      ------          >>>>>>>>>>            \        
//  \  /      \  /										 \        \   /
//   \/        \/											  \         \/
//  	-----------											 	 -----------




void mergeIstyleQuad(PolyMesh& theMesh,
	vtkSmartPointer<vtkPoints>&   thePoints,
	vtkSmartPointer<vtkPolyData>& thePolyData,
	vtkSmartPointer<vtkRenderWindow>& theRender)
{
	int adjustFlag = 1;
	while (adjustFlag) {
		adjustFlag = 0;


		std::vector<PolyMesh::EdgeHandle> faceEdges;
		findFaceEdges(theMesh, faceEdges);

		std::set<PolyMesh::VertexHandle> boundaryVertices;
		getEdgeVertices(theMesh, faceEdges, boundaryVertices);

		for (PolyMesh::FaceIter f_it = theMesh.faces_begin(); f_it != theMesh.faces_end(); ++f_it) {
			PolyMesh::FaceHandle fh = *f_it;
			if (theMesh.valence(fh) == 4)
			{
				int bounaryFlag = 0;
				for (PolyMesh::FaceEdgeIter fe_it = theMesh.fe_iter(fh); fe_it.is_valid(); ++fe_it) {
					PolyMesh::EdgeHandle eh = *fe_it;

					PolyMesh::HalfedgeHandle heh1 = theMesh.halfedge_handle(eh, 0);
					PolyMesh::HalfedgeHandle heh2 = theMesh.halfedge_handle(eh, 1);
					PolyMesh::FaceHandle fh1 = theMesh.face_handle(heh1);
					PolyMesh::FaceHandle fh2 = theMesh.face_handle(heh2);

					if (!fh1.is_valid() || (fh1.is_valid() && theMesh.valence(fh1)==3) || !fh2.is_valid() || (fh2.is_valid() && theMesh.valence(fh2) == 3)) {
						bounaryFlag = 1;
						break;
					}
				}
				if (bounaryFlag == 1) {
					continue;
				}
				std::vector< PolyMesh::VertexHandle > quadVertices;
				for (PolyMesh::FaceVertexIter fv_it = theMesh.fv_iter(fh); fv_it.is_valid(); ++fv_it) {
					quadVertices.push_back(*fv_it);
				}
				if (theMesh.valence(quadVertices[0]) == 3 && theMesh.valence(quadVertices[1]) == 3 && std::find(boundaryVertices.begin(), boundaryVertices.end(), quadVertices[0]) == boundaryVertices.end() && std::find(boundaryVertices.begin(), boundaryVertices.end(), quadVertices[1]) == boundaryVertices.end())
				{
#ifdef _DEBUG
					if (theMesh.is_valid_handle(quadVertices[0])) {
						PolyMesh::Point vh1_point = theMesh.point(quadVertices[0]);
						std::cout << "vh1_point : " << vh1_point << std::endl;
					}
					if (theMesh.is_valid_handle(quadVertices[1])) {
						PolyMesh::Point vh2_point = theMesh.point(quadVertices[1]);
						std::cout << "vh2_point : " << vh2_point << std::endl;
					}
#endif // _DEBUG

					PolyMesh::HalfedgeHandle heh = theMesh.find_halfedge(quadVertices[0], quadVertices[1]);

					PolyMesh::EdgeHandle eh = theMesh.edge_handle(heh);

					// 获取与边缘相邻的两个面
					PolyMesh::HalfedgeHandle heh1 = theMesh.halfedge_handle(eh, 0);
					PolyMesh::HalfedgeHandle heh2 = theMesh.halfedge_handle(eh, 1);
					PolyMesh::FaceHandle fh1 = theMesh.face_handle(heh1);
					PolyMesh::FaceHandle fh2 = theMesh.face_handle(heh2);

					unsigned int fh_val = 100;
					PolyMesh::FaceHandle fh_new;
					PolyMesh::VertexHandle vh_new;
					std::vector< PolyMesh::FaceHandle > fhs_new = { fh1 , fh2 };
					std::vector< PolyMesh::VertexHandle > vhs_new = { quadVertices[0] , quadVertices[1] };
					for (auto tempfh : fhs_new) {
						for (auto tempvh : vhs_new) {
							PolyMesh::VertexHandle opposite_tempvh = sortQuadVertices(theMesh, tempfh, tempvh)[3];
							unsigned int fh_val_tmp = theMesh.valence(opposite_tempvh);
							if (fh_val_tmp < fh_val && std::find(boundaryVertices.begin(), boundaryVertices.end(), opposite_tempvh) == boundaryVertices.end()) {
								fh_val = fh_val_tmp;
								fh_new = tempfh;
								vh_new = tempvh;
							}
						}
					}

					std::vector< PolyMesh::VertexHandle > quadVerticesNew = sortQuadVertices(theMesh, fh_new, vh_new);

					theMesh.request_vertex_status();
					theMesh.request_edge_status();
					theMesh.request_face_status();
					theMesh.delete_face(fh_new, false);
					theMesh.garbage_collection(false);

					autoAddFace(theMesh, quadVerticesNew[0], quadVerticesNew[3], quadVerticesNew[1]);
					autoAddFace(theMesh, quadVerticesNew[0], quadVerticesNew[3], quadVerticesNew[2]);


					PolyMesh::Point& p0 = theMesh.point(quadVerticesNew[0]);
					PolyMesh::Point& p1 = theMesh.point(quadVerticesNew[3]);
					PolyMesh::Point vector = p0 - p1;
					double distance = vector.norm();
					double move_distance = 0.5 * distance;
					PolyMesh::Point move_vector = vector.normalized() * move_distance;
					p1 += move_vector;

					PolyMesh::HalfedgeHandle halfEdgeHandle = theMesh.find_halfedge(quadVerticesNew[0], quadVerticesNew[3]);
					if (theMesh.is_valid_handle(halfEdgeHandle)) {
						theMesh.collapse(halfEdgeHandle);
						combineSingleTria(theMesh);
						combineQuadWithInteriorVertex(theMesh);
#ifdef _DEBUG						
						saveToVtkFile(theMesh, "adjust_after.vtk");
#endif // _DEBUG						
						adjustFlag = 1;
						break;
					}
				}
				else if (theMesh.valence(quadVertices[1]) == 3 && theMesh.valence(quadVertices[2]) == 3 && std::find(boundaryVertices.begin(), boundaryVertices.end(), quadVertices[1]) == boundaryVertices.end() && std::find(boundaryVertices.begin(), boundaryVertices.end(), quadVertices[2]) == boundaryVertices.end())
				{
#ifdef _DEBUG					
					if (theMesh.is_valid_handle(quadVertices[1])) {
						PolyMesh::Point vh1_point = theMesh.point(quadVertices[1]);
						std::cout << "vh1_point : " << vh1_point << std::endl;
					}
					if (theMesh.is_valid_handle(quadVertices[2])) {
						PolyMesh::Point vh2_point = theMesh.point(quadVertices[2]);
						std::cout << "vh2_point : " << vh2_point << std::endl;
					}
#endif // _DEBUG					
					PolyMesh::HalfedgeHandle heh = theMesh.find_halfedge(quadVertices[1], quadVertices[2]);

					PolyMesh::EdgeHandle eh = theMesh.edge_handle(heh);

					// 获取与边缘相邻的两个面
					PolyMesh::HalfedgeHandle heh1 = theMesh.halfedge_handle(eh, 0);
					PolyMesh::HalfedgeHandle heh2 = theMesh.halfedge_handle(eh, 1);
					PolyMesh::FaceHandle fh1 = theMesh.face_handle(heh1);
					PolyMesh::FaceHandle fh2 = theMesh.face_handle(heh2);

					unsigned int fh_val = 100;
					PolyMesh::FaceHandle fh_new;
					PolyMesh::VertexHandle vh_new;
					std::vector< PolyMesh::FaceHandle > fhs_new = { fh1 , fh2 };
					std::vector< PolyMesh::VertexHandle > vhs_new = { quadVertices[1] , quadVertices[2] };
					for (auto tempfh : fhs_new) {
						for (auto tempvh : vhs_new) {
							PolyMesh::VertexHandle opposite_tempvh = sortQuadVertices(theMesh, tempfh, tempvh)[3];
							unsigned int fh_val_tmp = theMesh.valence(opposite_tempvh);
							if (fh_val_tmp < fh_val && std::find(boundaryVertices.begin(), boundaryVertices.end(), opposite_tempvh) == boundaryVertices.end()) {
								fh_val = fh_val_tmp;
								fh_new = tempfh;
								vh_new = tempvh;
							}
						}
					}

					std::vector< PolyMesh::VertexHandle > quadVerticesNew = sortQuadVertices(theMesh, fh_new, vh_new);

					theMesh.request_vertex_status();
					theMesh.request_edge_status();
					theMesh.request_face_status();
					theMesh.delete_face(fh_new, false);
					theMesh.garbage_collection(false);

					autoAddFace(theMesh, quadVerticesNew[0], quadVerticesNew[3], quadVerticesNew[1]);
					autoAddFace(theMesh, quadVerticesNew[0], quadVerticesNew[3], quadVerticesNew[2]);


					PolyMesh::Point& p0 = theMesh.point(quadVerticesNew[0]);
					PolyMesh::Point& p1 = theMesh.point(quadVerticesNew[3]);
					PolyMesh::Point vector = p0 - p1;
					double distance = vector.norm();
					double move_distance = 0.5 * distance;
					PolyMesh::Point move_vector = vector.normalized() * move_distance;
					p1 += move_vector;

					PolyMesh::HalfedgeHandle halfEdgeHandle = theMesh.find_halfedge(quadVerticesNew[0], quadVerticesNew[3]);
					if (theMesh.is_valid_handle(halfEdgeHandle)) {
						theMesh.collapse(halfEdgeHandle);
						combineSingleTria(theMesh);
						combineQuadWithInteriorVertex(theMesh);
#ifdef _DEBUG						
						saveToVtkFile(theMesh, "adjust_after.vtk");
#endif // _DEBUG						
						adjustFlag = 1;
						break;
					}
				}
				else if (theMesh.valence(quadVertices[2]) == 3 && theMesh.valence(quadVertices[3]) == 3 && std::find(boundaryVertices.begin(), boundaryVertices.end(), quadVertices[2]) == boundaryVertices.end() && std::find(boundaryVertices.begin(), boundaryVertices.end(), quadVertices[3]) == boundaryVertices.end())
				{
#ifdef _DEBUG	
					if (theMesh.is_valid_handle(quadVertices[2])) {
						PolyMesh::Point vh1_point = theMesh.point(quadVertices[2]);
						std::cout << "vh1_point : " << vh1_point << std::endl;
					}
					if (theMesh.is_valid_handle(quadVertices[3])) {
						PolyMesh::Point vh2_point = theMesh.point(quadVertices[3]);
						std::cout << "vh2_point : " << vh2_point << std::endl;
					}
#endif // _DEBUG	
					PolyMesh::HalfedgeHandle heh = theMesh.find_halfedge(quadVertices[2], quadVertices[3]);

					PolyMesh::EdgeHandle eh = theMesh.edge_handle(heh);

					// 获取与边缘相邻的两个面
					PolyMesh::HalfedgeHandle heh1 = theMesh.halfedge_handle(eh, 0);
					PolyMesh::HalfedgeHandle heh2 = theMesh.halfedge_handle(eh, 1);
					PolyMesh::FaceHandle fh1 = theMesh.face_handle(heh1);
					PolyMesh::FaceHandle fh2 = theMesh.face_handle(heh2);

					unsigned int fh_val = 100;
					PolyMesh::FaceHandle fh_new;
					PolyMesh::VertexHandle vh_new;
					std::vector< PolyMesh::FaceHandle > fhs_new = { fh1 , fh2 };
					std::vector< PolyMesh::VertexHandle > vhs_new = { quadVertices[2] , quadVertices[3] };
					for (auto tempfh : fhs_new) {
						for (auto tempvh : vhs_new) {
							PolyMesh::VertexHandle opposite_tempvh = sortQuadVertices(theMesh, tempfh, tempvh)[3];
							unsigned int fh_val_tmp = theMesh.valence(opposite_tempvh);
							if (fh_val_tmp < fh_val && std::find(boundaryVertices.begin(), boundaryVertices.end(), opposite_tempvh) == boundaryVertices.end()) {
								fh_val = fh_val_tmp;
								fh_new = tempfh;
								vh_new = tempvh;
							}
						}
					}

					std::vector< PolyMesh::VertexHandle > quadVerticesNew = sortQuadVertices(theMesh, fh_new, vh_new);

					theMesh.request_vertex_status();
					theMesh.request_edge_status();
					theMesh.request_face_status();
					theMesh.delete_face(fh_new, false);
					theMesh.garbage_collection(false);

					autoAddFace(theMesh, quadVerticesNew[0], quadVerticesNew[3], quadVerticesNew[1]);
					autoAddFace(theMesh, quadVerticesNew[0], quadVerticesNew[3], quadVerticesNew[2]);

					PolyMesh::Point& p0 = theMesh.point(quadVerticesNew[0]);
					PolyMesh::Point& p1 = theMesh.point(quadVerticesNew[3]);
					PolyMesh::Point vector = p0 - p1;
					double distance = vector.norm();
					double move_distance = 0.5 * distance;
					PolyMesh::Point move_vector = vector.normalized() * move_distance;
					p1 += move_vector;

					PolyMesh::HalfedgeHandle halfEdgeHandle = theMesh.find_halfedge(quadVerticesNew[0], quadVerticesNew[3]);
					if (theMesh.is_valid_handle(halfEdgeHandle)) {
						theMesh.collapse(halfEdgeHandle);
						combineSingleTria(theMesh);
						combineQuadWithInteriorVertex(theMesh);
#ifdef _DEBUG						
						saveToVtkFile(theMesh, "adjust_after.vtk");
#endif // _DEBUG				
						adjustFlag = 1;
						break;
					}
				}
				else if (theMesh.valence(quadVertices[0]) == 3 && theMesh.valence(quadVertices[3]) == 3 && std::find(boundaryVertices.begin(), boundaryVertices.end(), quadVertices[0]) == boundaryVertices.end() && std::find(boundaryVertices.begin(), boundaryVertices.end(), quadVertices[3]) == boundaryVertices.end())
				{
#ifdef _DEBUG		
					if (theMesh.is_valid_handle(quadVertices[0])) {
						PolyMesh::Point vh1_point = theMesh.point(quadVertices[0]);
						std::cout << "vh1_point : " << vh1_point << std::endl;
					}
					if (theMesh.is_valid_handle(quadVertices[3])) {
						PolyMesh::Point vh2_point = theMesh.point(quadVertices[3]);
						std::cout << "vh2_point : " << vh2_point << std::endl;
					}
#endif // _DEBUG						
					PolyMesh::HalfedgeHandle heh = theMesh.find_halfedge(quadVertices[0], quadVertices[3]);

					PolyMesh::EdgeHandle eh = theMesh.edge_handle(heh);

					// 获取与边缘相邻的两个面
					PolyMesh::HalfedgeHandle heh1 = theMesh.halfedge_handle(eh, 0);
					PolyMesh::HalfedgeHandle heh2 = theMesh.halfedge_handle(eh, 1);
					PolyMesh::FaceHandle fh1 = theMesh.face_handle(heh1);
					PolyMesh::FaceHandle fh2 = theMesh.face_handle(heh2);

					unsigned int fh_val = 100;
					PolyMesh::FaceHandle fh_new;
					PolyMesh::VertexHandle vh_new;
					std::vector< PolyMesh::FaceHandle > fhs_new = { fh1 , fh2 };
					std::vector< PolyMesh::VertexHandle > vhs_new = { quadVertices[0] , quadVertices[3] };
					for (auto tempfh : fhs_new) {
						for (auto tempvh : vhs_new) {
							PolyMesh::VertexHandle opposite_tempvh = sortQuadVertices(theMesh, tempfh, tempvh)[3];
							unsigned int fh_val_tmp = theMesh.valence(opposite_tempvh);
							if (fh_val_tmp < fh_val && std::find(boundaryVertices.begin(), boundaryVertices.end(), opposite_tempvh) == boundaryVertices.end()) {
								fh_val = fh_val_tmp;
								fh_new = tempfh;
								vh_new = tempvh;
							}
						}
					}

					std::vector< PolyMesh::VertexHandle > quadVerticesNew = sortQuadVertices(theMesh, fh_new, vh_new);

					theMesh.request_vertex_status();
					theMesh.request_edge_status();
					theMesh.request_face_status();
					theMesh.delete_face(fh_new, false);
					theMesh.garbage_collection(false);

					autoAddFace(theMesh, quadVerticesNew[0], quadVerticesNew[3], quadVerticesNew[1]);
					autoAddFace(theMesh, quadVerticesNew[0], quadVerticesNew[3], quadVerticesNew[2]);

					PolyMesh::Point& p0 = theMesh.point(quadVerticesNew[0]);
					PolyMesh::Point& p1 = theMesh.point(quadVerticesNew[3]);
					PolyMesh::Point vector = p0 - p1;
					double distance = vector.norm();
					double move_distance = 0.5 * distance;
					PolyMesh::Point move_vector = vector.normalized() * move_distance;
					p1 += move_vector;

					PolyMesh::HalfedgeHandle halfEdgeHandle = theMesh.find_halfedge(quadVerticesNew[0], quadVerticesNew[3]);
					if (theMesh.is_valid_handle(halfEdgeHandle)) {
						theMesh.collapse(halfEdgeHandle);
						combineSingleTria(theMesh);
						combineQuadWithInteriorVertex(theMesh);
#ifdef _DEBUG						
						saveToVtkFile(theMesh, "adjust_after.vtk");
#endif // _DEBUG				
						adjustFlag = 1;
						break;
					}
				}
			}
		}
	}
	// jacobiLaplaceSmooth(theMesh);
#ifdef _DEBUG	
	updateVtkView(thePoints, thePolyData, theRender, theMesh);
#endif // _DEBUG
}













PolyMesh::VertexHandle getSwapSplitTria(PolyMesh& theMesh,
	const PolyMesh::VertexHandle& currentVH,
	const PolyMesh::VertexHandle& secondVH,
	const PolyMesh::VertexHandle& thirdVH,
	const PolyMesh::VertexHandle& leftVH,
	const PolyMesh::VertexHandle& rightVH,
	double threshAngle1) 
{

	PolyMesh::FaceHandle splitTriaHandle = getTriaByVertex(theMesh, currentVH, leftVH, rightVH);


	std::vector<PolyMesh::FaceHandle> leftVHtrias;
	for (PolyMesh::VertexFaceIter vf_it = theMesh.vf_iter(leftVH); vf_it.is_valid(); ++vf_it) {
		if (theMesh.valence(*vf_it) == 3) {
			leftVHtrias.push_back(*vf_it);
		}
	}
	std::vector<PolyMesh::FaceHandle> reflectTrias;
	for (PolyMesh::VertexFaceIter vf_it = theMesh.vf_iter(rightVH); vf_it.is_valid(); ++vf_it) {
		if (theMesh.valence(*vf_it) == 3 && 
			std::find(leftVHtrias.begin(), leftVHtrias.end(), *vf_it) != leftVHtrias.end())
		{
			reflectTrias.push_back(*vf_it);
		}
	}

	for (auto triaHandle : reflectTrias) {
		std::set<PolyMesh::VertexHandle> node_set;
		for (PolyMesh::FaceVertexIter fv_it = theMesh.fv_iter(splitTriaHandle); fv_it.is_valid(); ++fv_it) {
			node_set.insert(*fv_it);
		}
		for (PolyMesh::FaceVertexIter fv_it = theMesh.fv_iter(triaHandle); fv_it.is_valid(); ++fv_it) {
			node_set.insert(*fv_it);
		}
		node_set.insert(leftVH);
		node_set.insert(rightVH);
		if (node_set.size() == 4 &&
			std::find(node_set.begin(), node_set.end(), secondVH) == node_set.end() &&
			std::find(node_set.begin(), node_set.end(), thirdVH) == node_set.end()) {
			node_set.erase(leftVH);
			node_set.erase(rightVH);
			node_set.erase(currentVH);

			PolyMesh::VertexHandle reflectVH = *node_set.begin();

			double angle1 = getAngle(theMesh, reflectVH, currentVH, secondVH);
			double angle2 = getAngle(theMesh, reflectVH, currentVH, thirdVH);
			double length1 = getDistance(theMesh, reflectVH, currentVH);
			double length2 = (getDistance(theMesh, currentVH, secondVH) + getDistance(theMesh, currentVH, thirdVH))*0.75;
			if (angle1 >= threshAngle1 && angle2 >= threshAngle1 && length1 <= length2) {

				theMesh.request_vertex_status();
				theMesh.request_edge_status();
				theMesh.request_face_status();

				// 满足 swap 条件 变换三角形面
				theMesh.delete_face(splitTriaHandle, false);
				theMesh.delete_face(triaHandle, false);
				theMesh.garbage_collection();

				autoAddFace(theMesh, rightVH, reflectVH, currentVH);
				autoAddFace(theMesh, leftVH, reflectVH, currentVH);

				return reflectVH;
			}
			else {
				// 否则 split 创建四个新的三角形面

				PolyMesh::Point mid_point = (theMesh.point(leftVH) + theMesh.point(rightVH)) * 0.5;
				PolyMesh::VertexHandle new_vertex = theMesh.add_vertex(mid_point);

				theMesh.request_vertex_status();
				theMesh.request_edge_status();
				theMesh.request_face_status();

				theMesh.delete_face(splitTriaHandle, false);
				theMesh.delete_face(triaHandle, false);
				theMesh.garbage_collection(false);
				
				autoAddFace(theMesh, rightVH, currentVH, new_vertex);
				autoAddFace(theMesh, rightVH, reflectVH, new_vertex);
				autoAddFace(theMesh, leftVH, currentVH, new_vertex);
				autoAddFace(theMesh, leftVH, reflectVH, new_vertex);


				return new_vertex;
			}
		}
	}
	return PolyMesh::InvalidVertexHandle;
}






PolyMesh::VertexHandle getAlignVertex(PolyMesh& theMesh,
	const PolyMesh::VertexHandle& currentVH,
	const PolyMesh::VertexHandle& secondVH,
	const PolyMesh::VertexHandle& thirdVH) 
{

	std::vector<std::pair<PolyMesh::VertexHandle, double>> vertexAngleList = 
		getVertexAngleList(theMesh, currentVH, secondVH, thirdVH);

	double lastValue = vertexAngleList.back().second;

	if (lastValue < 40) 
	{ 
		return PolyMesh::InvalidVertexHandle;
	}
	else if (lastValue <= 125) 
	{
		if (!checkTriaByVertex(theMesh, currentVH, 
			vertexAngleList.back().first,
			vertexAngleList[vertexAngleList.size() - 2].first)) 
		{
			return vertexAngleList[vertexAngleList.size() - 2].first;
		}
		else {
			return vertexAngleList.back().first;
		}
	}
	else if (lastValue <= 200) 
	{
		double bisectorAngle = lastValue / 2.0;
		double threshAngle1 = bisectorAngle - 23.0;
		double threshAngle2 = bisectorAngle + 23.0;
		for (auto vh_angPair : vertexAngleList) {
			if (vh_angPair.second >= threshAngle1 && (lastValue - vh_angPair.second) >= threshAngle1) {
				return vh_angPair.first;
			}
		}
		PolyMesh::VertexHandle preVertex;
		for (auto vh_angPair : vertexAngleList) {
			if (vh_angPair.second > threshAngle2) {
				return getSwapSplitTria(theMesh, currentVH, secondVH, thirdVH, preVertex, vh_angPair.first, threshAngle1);
			}
			preVertex = vh_angPair.first;
		}
		return PolyMesh::InvalidVertexHandle;
	}
	else {
		int minAngle = 1000;
		PolyMesh::VertexHandle minVertex;
		for (auto vh_angPair : vertexAngleList) {
			double tempAngle = fabs(vh_angPair.second - 90.0);
			if (tempAngle < minAngle) {
				minAngle = tempAngle;
				minVertex = vh_angPair.first;
			}
		}
		return minVertex;
	}
}





int checkQuadFeasibility(PolyMesh& theMesh, 
	const PolyMesh::VertexHandle& vh1,
	const PolyMesh::VertexHandle& vh2,
	const PolyMesh::VertexHandle& vh3,
	const PolyMesh::VertexHandle& vh4)
{

	int edgeNum = 0;
	int nodeNum = 0;

	std::vector<std::vector<PolyMesh::VertexHandle>> quadEdges = 
		{ {vh1, vh2}, {vh2, vh3} , {vh3, vh4} ,{vh4, vh1} };

	for (auto edgeVertices : quadEdges) {
		for (PolyMesh::VertexEdgeIter ve_it = theMesh.ve_iter(edgeVertices[0]); ve_it.is_valid(); ++ve_it) {
			PolyMesh::HalfedgeHandle heh1 = theMesh.halfedge_handle(*ve_it, 0);
			PolyMesh::HalfedgeHandle heh2 = theMesh.halfedge_handle(*ve_it, 1);
			PolyMesh::VertexHandle vh1 = theMesh.to_vertex_handle(heh1);
			PolyMesh::VertexHandle vh2 = theMesh.to_vertex_handle(heh2);

			if (vh1 == edgeVertices[1] || vh2 == edgeVertices[1]) {
				PolyMesh::FaceHandle fh1 = theMesh.face_handle(heh1);
				PolyMesh::FaceHandle fh2 = theMesh.face_handle(heh2);
				// 如果只有一个三角形面与边缘相邻，则输出边缘编号
				if (fh1.is_valid() && theMesh.valence(fh1) == 3 && 
						(!fh2.is_valid() || (fh2.is_valid() && theMesh.valence(fh2) == 4)))
				{
					edgeNum++;
				}
				else if (fh2.is_valid() && theMesh.valence(fh2) == 3 && 
						(!fh1.is_valid() || (fh1.is_valid() && theMesh.valence(fh1) == 4)))
				{
					edgeNum++;
				}
				break;
			}
		}
	}

	std::vector<PolyMesh::VertexHandle > quadVertices = { vh1, vh2, vh3, vh4 };

	for (auto vh : quadVertices) {
		for (PolyMesh::VertexEdgeIter ve_it = theMesh.ve_iter(vh); ve_it.is_valid(); ++ve_it) {
			PolyMesh::EdgeHandle eh = *ve_it;
			// 获取与边缘相邻的两个面
			PolyMesh::HalfedgeHandle heh1 = theMesh.halfedge_handle(eh, 0);
			PolyMesh::HalfedgeHandle heh2 = theMesh.halfedge_handle(eh, 1);
			PolyMesh::FaceHandle fh1 = theMesh.face_handle(heh1);
			PolyMesh::FaceHandle fh2 = theMesh.face_handle(heh2);
			// 如果只有一个三角形面与边缘相邻，则输出边缘编号
			if (fh1.is_valid() && theMesh.valence(fh1) == 3 && 
				(!fh2.is_valid() || (fh2.is_valid() && theMesh.valence(fh2) == 4)))
			{
				nodeNum++;
				break;
			}
			else if (fh2.is_valid() && theMesh.valence(fh2) == 3 && 
				(!fh1.is_valid() || (fh1.is_valid() && theMesh.valence(fh1) == 4)))
			{
				nodeNum++;
				break;
			}
		}
	}

	if (nodeNum == 2) {
		return 1;
	}
	else if (nodeNum == 3 && edgeNum == 2) {
		return 1;
	}
	else if (nodeNum == 4 && edgeNum > 2) {
		return 1;
	}
	else if (nodeNum == 4 && edgeNum == 2) {
		return -1;
	}
	else {
		return 0;
	}
}






void combineSingleTria(PolyMesh& theMesh) 
{
	int quadNum = 0;

	std::vector<PolyMesh::EdgeHandle> boundaryEdges;

	findTriaEdges(theMesh, boundaryEdges);

	std::set<PolyMesh::VertexHandle> boundaryVertices;

	getEdgeVertices(theMesh, boundaryEdges, boundaryVertices);

	int combineFlag = 1;
	while (combineFlag) {
		combineFlag = 0;
		for (auto vh : boundaryVertices) {
			std::vector<PolyMesh::FaceHandle> adjTriaFaces;
			for (PolyMesh::VertexFaceIter vf_it = theMesh.vf_iter(vh); vf_it.is_valid(); ++vf_it) {
				if (theMesh.valence(*vf_it) == 3) {
					adjTriaFaces.push_back(*vf_it);
				}
			}
			std::vector<PolyMesh::EdgeHandle> adjEdges;
			for (PolyMesh::VertexEdgeIter ve_it = theMesh.ve_iter(vh); ve_it.is_valid(); ++ve_it) {
				if (std::find(boundaryEdges.begin(), boundaryEdges.end(), *ve_it) != boundaryEdges.end()) {
					adjEdges.push_back(*ve_it);
				}
			}

			if (adjEdges.size() == adjTriaFaces.size() * 2) {
				for (auto triaFace : adjTriaFaces) {

					// 获取当前三角形面的三个节点
					std::vector<PolyMesh::VertexHandle> triaVertices;
					for (PolyMesh::FaceVertexIter fv_it = theMesh.fv_iter(triaFace); fv_it.is_valid(); ++fv_it) {
						triaVertices.push_back(*fv_it);
					}
					// 从当前当前三角形面的三个节点中去除当前节点
					triaVertices.erase(std::remove(triaVertices.begin(), triaVertices.end(), vh), triaVertices.end());
					
					// 获取对称三角形面
					std::vector<PolyMesh::FaceHandle> leftVHtrias;
					for (PolyMesh::VertexFaceIter vf_it = theMesh.vf_iter(triaVertices[0]); vf_it.is_valid(); ++vf_it) {
						if (theMesh.valence(*vf_it) == 3) {
							leftVHtrias.push_back(*vf_it);
						}
					}
					std::vector<PolyMesh::FaceHandle> adjTriaFaces_new;
					for (PolyMesh::VertexFaceIter vf_it = theMesh.vf_iter(triaVertices[1]); vf_it.is_valid(); ++vf_it) {
						if (theMesh.valence(*vf_it) == 3 && 
							std::find(leftVHtrias.begin(), leftVHtrias.end(), *vf_it) != leftVHtrias.end())
						{
							adjTriaFaces_new.push_back(*vf_it);
						}
					}
					
					for (auto triaFace_new : adjTriaFaces_new) {
						// 从所在的三角形面中找到当前三角形面关于剩下的节点对称的三角形面
						std::set<PolyMesh::VertexHandle> triasVertices;
						for (PolyMesh::FaceVertexIter fv_it = theMesh.fv_iter(triaFace); fv_it.is_valid(); ++fv_it) {
							triasVertices.insert(*fv_it);
						}
						for (PolyMesh::FaceVertexIter fv_it = theMesh.fv_iter(triaFace_new); fv_it.is_valid(); ++fv_it) {
							triasVertices.insert(*fv_it);
						}
						triasVertices.insert(triaVertices[0]);
						triasVertices.insert(triaVertices[1]);
						if (triasVertices.size() == 4) {
							std::vector<PolyMesh::VertexHandle> triaVertices_new;
							for (PolyMesh::FaceVertexIter fv_it = theMesh.fv_iter(triaFace_new); fv_it.is_valid(); ++fv_it) {
								triaVertices_new.push_back(*fv_it);
							}
							triaVertices_new.erase(std::remove(triaVertices_new.begin(), triaVertices_new.end(), triaVertices[0]), triaVertices_new.end());
							triaVertices_new.erase(std::remove(triaVertices_new.begin(), triaVertices_new.end(), triaVertices[1]), triaVertices_new.end());

							// 获取找到的三角形面中与当前节点对称的节点
							PolyMesh::VertexHandle reflectVertex = triaVertices_new[0];

							if (checkQuadFeasibility(theMesh, vh, triaVertices[0], reflectVertex, triaVertices[1]) == 1 &&
								getFaceAngle(theMesh, triaVertices[0], triaVertices[1], vh, reflectVertex) > 90.0 )
							{
								theMesh.request_vertex_status();
								theMesh.request_edge_status();
								theMesh.request_face_status();

								theMesh.delete_face(triaFace_new, false);
								theMesh.delete_face(triaFace, false);
								theMesh.garbage_collection(false);
								
								PolyMesh::FaceHandle newQuad;
								autoAddFace(theMesh, vh, triaVertices[0], reflectVertex, triaVertices[1], newQuad);
								
								
								// 防止出现两个四边形单元拥有三个共同节点的情况
								std::set<PolyMesh::FaceHandle> quadFaces_new;
								
								for (PolyMesh::VertexFaceIter vf_it = theMesh.vf_iter(vh); vf_it.is_valid(); ++vf_it) {
									if (theMesh.is_valid_handle(*vf_it) && theMesh.valence(*vf_it) == 4) {
										quadFaces_new.insert(*vf_it);
									}
								}
								

								for (auto quadFace_new : quadFaces_new) {
									std::set<PolyMesh::VertexHandle> quadsVertices;
									quadsVertices.insert(vh);
									quadsVertices.insert(triaVertices[0]);
									quadsVertices.insert(reflectVertex);
									quadsVertices.insert(triaVertices[1]);
									for (PolyMesh::FaceVertexIter fv_it = theMesh.fv_iter(quadFace_new); fv_it.is_valid(); ++fv_it) {
										quadsVertices.insert(*fv_it);
									}
									if (quadsVertices.size() == 5) {

										quadsVertices.erase(vh);
										quadsVertices.erase(triaVertices[0]);
										quadsVertices.erase(reflectVertex);
										quadsVertices.erase(triaVertices[1]);

										PolyMesh::VertexHandle restVertex = *quadsVertices.begin();

										theMesh.request_vertex_status();
										theMesh.request_edge_status();
										theMesh.request_face_status();

										PolyMesh::FaceHandle qh;
										if (theMesh.valence(triaVertices[0]) == 2) {
											

											theMesh.delete_face(newQuad, false);
											theMesh.delete_face(quadFace_new, false);
											theMesh.garbage_collection(false);
											
											autoAddFace(theMesh, vh, triaVertices[1], reflectVertex, restVertex, qh);
										}
										else if (theMesh.valence(triaVertices[1]) == 2) {
											

											theMesh.delete_face(newQuad, false);
											theMesh.delete_face(quadFace_new, false);
											theMesh.garbage_collection(false);

											autoAddFace(theMesh, vh, triaVertices[0], reflectVertex, restVertex, qh);
										}
										break;
									}
								}
								

								quadNum++;
								combineFlag = 1;
								break;
							} 
							else if(checkQuadFeasibility(theMesh, vh, triaVertices[0], reflectVertex, triaVertices[1]) == -1 && getFaceAngle(theMesh, triaVertices[0], triaVertices[1], vh, reflectVertex) > 90.0 && getFaceAngle(theMesh, vh, reflectVertex, triaVertices[0], triaVertices[1]) > 90.0)
							{
								if (getTriaQualityFactor(theMesh, triaFace_new) >= 0.36) {
									OpenMesh::Vec3f center(0, 0, 0);
									PolyMesh::HalfedgeHandle heh = theMesh.halfedge_handle(triaFace_new);
									int valence = theMesh.valence(triaFace_new);
									for (int i = 0; i < valence; i++) {
										center += theMesh.point(theMesh.to_vertex_handle(heh));
										heh = theMesh.next_halfedge_handle(heh);
									}
									center /= valence;


									theMesh.request_vertex_status();
									theMesh.request_edge_status();
									theMesh.request_face_status();
									// 在中心点处创建一个新的节点
									PolyMesh::VertexHandle vh = theMesh.add_vertex(center);
									theMesh.split(triaFace_new, vh);
									theMesh.garbage_collection(false);
#ifdef _DEBUG	
									saveToVtkFile(theMesh, "split_after.vtk");
#endif // _DEBUG										
									combineFlag = 1;
									break;
								}
							}

							
						}
					}
				}
#ifdef _DEBUG
				saveToVtkFile(theMesh, "combine_after.vtk");
#endif // _DEBUG
				if (combineFlag == 1) { break; }
			}
			else {
				// 对每个三角形面判断是否可以combine，目前没有遇到
			}
		}
		

		boundaryEdges.clear();
		boundaryEdges.shrink_to_fit();
		boundaryVertices.clear();

		findTriaEdges(theMesh, boundaryEdges);

		getEdgeVertices(theMesh, boundaryEdges, boundaryVertices);
	}
#ifdef _DEBUG
	saveToVtkFile(theMesh, "combine_after.vtk");
#endif // _DEBUG
}







void combineQuadWithInteriorVertex(PolyMesh& theMesh) {
	
	int combineFlag = 1;
	while (combineFlag) {
		combineFlag = 0;

		std::vector<PolyMesh::EdgeHandle> faceEdges;
		findFaceEdges(theMesh, faceEdges);

		std::set<PolyMesh::VertexHandle> boundaryVertices;
		getEdgeVertices(theMesh, faceEdges, boundaryVertices);

		for (PolyMesh::VertexIter v_it = theMesh.vertices_begin(); v_it != theMesh.vertices_end(); ++v_it)
		{
			PolyMesh::VertexHandle vh = *v_it;
			if (theMesh.valence(vh) == 2 && std::find(boundaryVertices.begin(), boundaryVertices.end(), vh) == boundaryVertices.end()) {
				std::vector<PolyMesh::FaceHandle> quadFaces;
				for (PolyMesh::VertexFaceIter vf_it = theMesh.vf_iter(vh); vf_it.is_valid(); ++vf_it) {
					if (theMesh.is_valid_handle(*vf_it) && theMesh.valence(*vf_it) == 4) {
						quadFaces.push_back(*vf_it);
					}
				}

				if (quadFaces.size() == 2) {
					std::set<PolyMesh::VertexHandle> quadsVertices;
					for (PolyMesh::FaceVertexIter fv_it = theMesh.fv_iter(quadFaces[0]); fv_it.is_valid(); ++fv_it) {
						quadsVertices.insert(*fv_it);
					}
					for (PolyMesh::FaceVertexIter fv_it = theMesh.fv_iter(quadFaces[1]); fv_it.is_valid(); ++fv_it) {
						quadsVertices.insert(*fv_it);
					}
					if (quadsVertices.size() == 5) {
						std::vector<PolyMesh::VertexHandle> quadVertices1;
						for (PolyMesh::FaceVertexIter fv_it = theMesh.fv_iter(quadFaces[0]); fv_it.is_valid(); ++fv_it) {
							quadVertices1.push_back(*fv_it);
						}
						std::vector<PolyMesh::VertexHandle> quadVertices1_new = sortQuadVertices(quadVertices1, vh);

						std::vector<PolyMesh::VertexHandle> quadVertices2;
						for (PolyMesh::FaceVertexIter fv_it = theMesh.fv_iter(quadFaces[1]); fv_it.is_valid(); ++fv_it) {
							quadVertices2.push_back(*fv_it);
						}
						std::vector<PolyMesh::VertexHandle> quadVertices2_new = sortQuadVertices(quadVertices2, vh);


						theMesh.request_vertex_status();
						theMesh.request_edge_status();
						theMesh.request_face_status();

						theMesh.delete_face(quadFaces[0], false);
						theMesh.delete_face(quadFaces[1], false);
						theMesh.garbage_collection(false);




						PolyMesh::FaceHandle qh;
						autoAddFace(theMesh, quadVertices1_new[3], quadVertices1_new[1], quadVertices2_new[3], quadVertices1_new[2], qh);
						combineFlag = 1;
						break;
					}
				}
			}
		}
	}
}










bool checkQuadNodes(PolyMesh& theMesh, 
	const PolyMesh::VertexHandle& vh1, 
	const PolyMesh::VertexHandle& vh2) 
{

	std::vector<PolyMesh::FaceHandle> adjQuadFaces;

	for (PolyMesh::VertexFaceIter vf_it = theMesh.vf_iter(vh1); vf_it.is_valid(); ++vf_it) {
		if (theMesh.valence(*vf_it) == 4) {
			adjQuadFaces.push_back(*vf_it);
		}
	}

	for (auto qh : adjQuadFaces) {
		std::set<PolyMesh::VertexHandle> quadsVertices;
		for (PolyMesh::FaceVertexIter fv_it = theMesh.fv_iter(qh); fv_it.is_valid(); ++fv_it) {
			quadsVertices.insert(*fv_it);
		}
		if (std::find(quadsVertices.begin(), quadsVertices.end(), vh2) != quadsVertices.end()) {
			return true;
		}
	}
	return false;
}














void mergeSingleTriaNew(PolyMesh& theMesh) {


	std::vector<PolyMesh::EdgeHandle> boundaryEdges;
	findTriaEdges(theMesh, boundaryEdges);
#ifdef _DEBUG	
	std::cout << "------------------------------------" << std::endl;
	std::cout << "number of edges: " << boundaryEdges.size() << std::endl;
	std::cout << "------------------------------------" << std::endl;
#endif // _DEBUG		
	std::set<PolyMesh::VertexHandle> boundaryVertices;
	getEdgeVertices(theMesh, boundaryEdges, boundaryVertices);


	std::vector<PolyMesh::EdgeHandle> faceEdges;
	findFaceEdges(theMesh, faceEdges);

	std::set<PolyMesh::VertexHandle> initBoundaryVH;
	getEdgeVertices(theMesh, faceEdges, initBoundaryVH);

	int mergeFlag = 1;
	while (mergeFlag) {
		mergeFlag = 0;
		for (auto vh : boundaryVertices) {


			std::vector<PolyMesh::FaceHandle> adjTriaFaces;
			for (PolyMesh::VertexFaceIter vf_it = theMesh.vf_iter(vh); vf_it.is_valid(); ++vf_it) {
				if (theMesh.valence(*vf_it) == 3) {
					adjTriaFaces.push_back(*vf_it);
				}
			}

			if (adjTriaFaces.size() == 1) {
				PolyMesh::FaceHandle triaFace = adjTriaFaces[0];

				// 获取当前三角形面的三个节点
				std::vector<PolyMesh::VertexHandle> triaVertices;
				for (PolyMesh::FaceVertexIter fv_it = theMesh.fv_iter(triaFace); fv_it.is_valid(); ++fv_it) {
					triaVertices.push_back(*fv_it);
				}
				// 从当前当前三角形面的三个节点中去除当前节点
				triaVertices.erase(std::remove(triaVertices.begin(), triaVertices.end(), vh), triaVertices.end());

				double vhAngle = getAngle(theMesh, triaVertices[0], vh, triaVertices[1]);

				if (vhAngle < 40.0 &&
					checkQuadNodes(theMesh, triaVertices[0], vh) &&
					checkQuadNodes(theMesh, triaVertices[1], vh) &&
					std::find(initBoundaryVH.begin(), initBoundaryVH.end(), triaVertices[0]) == initBoundaryVH.end() &&
					std::find(initBoundaryVH.begin(), initBoundaryVH.end(), triaVertices[1]) == initBoundaryVH.end())
				{
					theMesh.request_vertex_status();
					theMesh.request_edge_status();
					theMesh.request_face_status();

					PolyMesh::Point& p0 = theMesh.point(triaVertices[0]);
					PolyMesh::Point& p1 = theMesh.point(triaVertices[1]);
					PolyMesh::Point vector = p0 - p1;
					double distance = vector.norm();
					double move_distance = 0.5 * distance;
					PolyMesh::Point move_vector = vector.normalized() * move_distance;
					p1 += move_vector;


					PolyMesh::HalfedgeHandle halfEdgeHandle = theMesh.find_halfedge(triaVertices[0], triaVertices[1]);
					if (theMesh.is_valid_handle(halfEdgeHandle)) {
						theMesh.collapse(halfEdgeHandle);
					}
					theMesh.garbage_collection();

					if (theMesh.valence(vh) == 2) {
						std::vector<PolyMesh::FaceHandle> quadFaces;
						for (PolyMesh::VertexFaceIter vf_it = theMesh.vf_iter(vh); vf_it.is_valid(); ++vf_it) {
							if (theMesh.is_valid_handle(*vf_it) && theMesh.valence(*vf_it) == 4) {
								quadFaces.push_back(*vf_it);
							}
						}
						if (quadFaces.size() == 2) {

							std::set<PolyMesh::VertexHandle> quadsVertices;
							for (PolyMesh::FaceVertexIter fv_it = theMesh.fv_iter(quadFaces[0]); fv_it.is_valid(); ++fv_it) {
								quadsVertices.insert(*fv_it);
							}
							for (PolyMesh::FaceVertexIter fv_it = theMesh.fv_iter(quadFaces[1]); fv_it.is_valid(); ++fv_it) {
								quadsVertices.insert(*fv_it);
							}
							if (quadsVertices.size() == 5) {
								std::vector<PolyMesh::VertexHandle> quadVertices1;
								for (PolyMesh::FaceVertexIter fv_it = theMesh.fv_iter(quadFaces[0]); fv_it.is_valid(); ++fv_it) {
									quadVertices1.push_back(*fv_it);
								}
								std::vector<PolyMesh::VertexHandle> quadVertices1_new = sortQuadVertices(quadVertices1, vh);

								std::vector<PolyMesh::VertexHandle> quadVertices2;
								for (PolyMesh::FaceVertexIter fv_it = theMesh.fv_iter(quadFaces[1]); fv_it.is_valid(); ++fv_it) {
									quadVertices2.push_back(*fv_it);
								}
								std::vector<PolyMesh::VertexHandle> quadVertices2_new = sortQuadVertices(quadVertices2, vh);


								theMesh.request_vertex_status();
								theMesh.request_edge_status();
								theMesh.request_face_status();

								theMesh.delete_face(quadFaces[0], false);
								theMesh.delete_face(quadFaces[1], false);
								theMesh.garbage_collection();
								PolyMesh::FaceHandle qh;
								autoAddFace(theMesh, quadVertices1_new[3], quadVertices1_new[1], quadVertices2_new[3], quadVertices1_new[2], qh);
							}
						}
					}
					mergeFlag = 1;
					break;
				}
			}
			
		}

		boundaryEdges.clear();
		boundaryEdges.shrink_to_fit();
		boundaryVertices.clear();
		
		findTriaEdges(theMesh, boundaryEdges);
		getEdgeVertices(theMesh, boundaryEdges, boundaryVertices);
	}


}



void autoAddFace(PolyMesh& theMesh,	
	const PolyMesh::VertexHandle& vh1,
	const PolyMesh::VertexHandle& vh2,
	const PolyMesh::VertexHandle& vh3)
{
	PolyMesh::FaceHandle th = theMesh.add_face(vh1, vh2, vh3);
	if (!theMesh.is_valid_handle(th)) {
		th = theMesh.add_face(vh1, vh3, vh2);
	}
}



void autoAddFace(PolyMesh& theMesh,
	const PolyMesh::VertexHandle& vh1,
	const PolyMesh::VertexHandle& vh2,
	const PolyMesh::VertexHandle& vh3,
	const PolyMesh::VertexHandle& vh4,
	PolyMesh::FaceHandle& theFH)
{
	theFH = theMesh.add_face(vh1, vh2, vh3, vh4);
	if (!theMesh.is_valid_handle(theFH)) {
		theFH = theMesh.add_face(vh1, vh4, vh3, vh2);
	}
}



void mergeSplitedTria(PolyMesh& theMesh) {

	std::vector<PolyMesh::EdgeHandle> faceEdges;
	findFaceEdges(theMesh, faceEdges);

	std::set<PolyMesh::VertexHandle> boundaryVertices;
	getEdgeVertices(theMesh, faceEdges, boundaryVertices);

	for (PolyMesh::VertexIter v_it = theMesh.vertices_begin(); v_it != theMesh.vertices_end(); ++v_it) {
		if (theMesh.valence(*v_it) == 3 && std::find(boundaryVertices.begin(), boundaryVertices.end(), *v_it) == boundaryVertices.end()) {
			std::vector<PolyMesh::FaceHandle> triaFaces;
			for (PolyMesh::VertexFaceIter vf_it = theMesh.vf_iter(*v_it); vf_it.is_valid(); ++vf_it) {
				if (theMesh.is_valid_handle(*vf_it) && theMesh.valence(*vf_it) == 3) {
					triaFaces.push_back(*vf_it);
				}
			}

			if (triaFaces.size() == 3) {
				std::set<PolyMesh::VertexHandle> triasVertices;
				for (auto th : triaFaces) {
					for (PolyMesh::FaceVertexIter fv_it = theMesh.fv_iter(th); fv_it.is_valid(); ++fv_it) {
						triasVertices.insert(*fv_it);
					}
				}
				triasVertices.erase(*v_it);
				theMesh.request_vertex_status();
				theMesh.request_edge_status();
				theMesh.request_face_status();

				theMesh.delete_face(triaFaces[0], false);
				theMesh.delete_face(triaFaces[1], false);
				theMesh.delete_face(triaFaces[2], false);
				theMesh.garbage_collection();

				std::vector<PolyMesh::VertexHandle> temptriasVertices;
				for (auto vh : triasVertices) {
					temptriasVertices.push_back(vh);
				}
				autoAddFace(theMesh, temptriasVertices[0], temptriasVertices[1], temptriasVertices[2]);
				break;
			}
		}
	}
}





void qmorphTriaToQuad(PolyMesh& theMesh,
	vtkSmartPointer<vtkPoints>&   thePoints,
	vtkSmartPointer<vtkPolyData>& thePolyData,
	vtkSmartPointer<vtkRenderWindow>& theRender)
{
	int fileIndex = 0;

	while (checkIsolatedTriangle(theMesh)) {
		fileIndex++;
		std::string baseFilePre = "input_";
		std::string fileType = ".vtk";
		std::string fileNameInput = baseFilePre + std::to_string(fileIndex) + fileType;
		
		// 先合并模型中的 single tria
		combineSingleTria(theMesh);
		mergeSingleTriaNew(theMesh);
		// jacobiLaplaceSmooth(theMesh);

		std::vector<PolyMesh::EdgeHandle> boundaryEdges;
		findTriaEdges(theMesh, boundaryEdges);

		if (boundaryEdges.size() == 0) {
			combineQuadWithInteriorVertex(theMesh);
			// jacobiLaplaceSmooth(theMesh);
			break;
		}

		std::set<PolyMesh::VertexHandle> boundaryVertices;
		getEdgeVertices(theMesh, boundaryEdges, boundaryVertices);

		// 获取 currentEdge group中平均长度最小的 currentEdges loop
		std::vector<PolyMesh::EdgeHandle> currentEdges = getGroupBoundaryEdges(theMesh, boundaryEdges);
		std::set<PolyMesh::VertexHandle> vertices;
		getEdgeVertices(theMesh, currentEdges, vertices);

		std::vector<PolyMesh::EdgeHandle> processedEdges;
		int connectFlag = 0;

		PolyMesh::EdgeHandle currentEdge = currentEdges[0];

		while (1) {
			


			PolyMesh::FaceHandle quadHandle;
			PolyMesh::EdgeHandle tempEdge;
			int quadFlag = 0;

			processedEdges.push_back(currentEdge);


			PolyMesh::HalfedgeHandle heh1 = theMesh.halfedge_handle(currentEdge, 0);
			PolyMesh::HalfedgeHandle heh2 = theMesh.halfedge_handle(currentEdge, 1);
			PolyMesh::VertexHandle vh1 = theMesh.to_vertex_handle(heh1);
			PolyMesh::VertexHandle vh2 = theMesh.to_vertex_handle(heh2);

			PolyMesh::VertexHandle vh1_third = getThirdVertex(theMesh, vh1, vh2, currentEdges);
			PolyMesh::VertexHandle vh2_third = getThirdVertex(theMesh, vh2, vh1, currentEdges);



			PolyMesh::Point vh1_point = theMesh.point(vh1);
			PolyMesh::Point vh2_point = theMesh.point(vh2);
			
#ifdef _DEBUG	
			std::cout << "**********current edge**********" << std::endl;
			int qqq = currentEdge.idx();
			std::cout << currentEdge.idx() << std::endl;
			std::cout << "vh1_point : " << vh1_point << std::endl;
			std::cout << "vh2_point : " << vh2_point << std::endl;
			if (theMesh.is_valid_handle(vh1_third)) {
				PolyMesh::Point vh1_third_point = theMesh.point(vh1_third);
				std::cout << "vh1_third_point : " << vh1_third_point << std::endl;
			}
			if (theMesh.is_valid_handle(vh2_third)) {
				PolyMesh::Point vh2_third_point = theMesh.point(vh2_third);

				std::cout << "vh2_third_point : " << vh2_third_point << std::endl;
			}
			std::string baseFileName = "qmorph_";
			std::string fileExtension = ".vtk";
			std::string fileName = baseFileName + std::to_string(qqq) + fileExtension;
#endif // _DEBUG	

			// 如果edge的两个节点所连接的edge数目不等于2，break
			// 如果edge的两个节点所连接的tria数目等于0，break
			{
				int vh1EdgeNum = 0;
				for (PolyMesh::VertexEdgeIter ve_it = theMesh.ve_iter(vh1); ve_it.is_valid(); ++ve_it) {
					if (std::find(boundaryEdges.begin(), boundaryEdges.end(), *ve_it) != boundaryEdges.end()) {
						vh1EdgeNum++;
					}
				}
				int vh2EdgeNum = 0;
				for (PolyMesh::VertexEdgeIter ve_it = theMesh.ve_iter(vh2); ve_it.is_valid(); ++ve_it) {
					if (std::find(boundaryEdges.begin(), boundaryEdges.end(), *ve_it) != boundaryEdges.end()) {
						vh2EdgeNum++;
					}
				}
				if (vh1EdgeNum != 2 || vh2EdgeNum != 2) {
					break;
				}


				int vh1TriaNum = 0;
				for (PolyMesh::VertexFaceIter vf_it = theMesh.vf_iter(vh1); vf_it.is_valid(); ++vf_it) {
					if (theMesh.valence(*vf_it) == 3) {
						vh1TriaNum++;
					}
				}
				int vh2TriaNum = 0;
				for (PolyMesh::VertexFaceIter vf_it = theMesh.vf_iter(vh2); vf_it.is_valid(); ++vf_it) {
					if (theMesh.valence(*vf_it) == 3) {
						vh2TriaNum++;
					}
				}
				if (vh1TriaNum == 0 || vh2TriaNum == 0) {
					break;
				}
			}

			

			// 凹陷edge
			if (getVertexAngleList(theMesh, vh1, vh2, vh1_third).back().second > 200.0 ||
				getVertexAngleList(theMesh, vh2, vh1, vh2_third).back().second > 200.0) {

				std::set<PolyMesh::VertexHandle> vh1Vertices;
				for (PolyMesh::VertexFaceIter vf_it = theMesh.vf_iter(vh1); vf_it.is_valid(); ++vf_it) {
					if (theMesh.valence(*vf_it) == 3) {
						for (PolyMesh::FaceVertexIter fv_it = theMesh.fv_iter(*vf_it); fv_it.is_valid(); ++fv_it) {
							vh1Vertices.insert(*fv_it);
						}
					}
				}
				vh1Vertices.erase(vh1);
				vh1Vertices.erase(vh2);
				vh1Vertices.erase(vh1_third);

				std::set<PolyMesh::VertexHandle> vh2Vertices;
				for (PolyMesh::VertexFaceIter vf_it = theMesh.vf_iter(vh2); vf_it.is_valid(); ++vf_it) {
					if (theMesh.valence(*vf_it) == 3) {
						for (PolyMesh::FaceVertexIter fv_it = theMesh.fv_iter(*vf_it); fv_it.is_valid(); ++fv_it) {
							vh2Vertices.insert(*fv_it);
						}
					}
				}
				vh2Vertices.erase(vh1);
				vh2Vertices.erase(vh2);
				vh2Vertices.erase(vh2_third);


				if (vh1Vertices.size() > 0 && vh2Vertices.size() > 0) {
					double toQuadQuality = 0.0;
					double realQuadQuality = 0.0;
					std::vector<PolyMesh::VertexHandle> toQuadVertices;
					for (auto vh1Vertex : vh1Vertices) {
						for (auto vh2Vertex : vh2Vertices) {
							if (vh1Vertex != vh2Vertex) {
								if (checkVetexConnection(theMesh, vh1Vertex, vh2Vertex) &&
									getFaceAngle(theMesh, vh1, vh2Vertex, vh2, vh1Vertex) > 90.0 &&
									getFaceAngle(theMesh, vh2, vh1Vertex, vh1, vh2Vertex) > 90.0) {

									if (checkQuadFeasibility(theMesh, vh1, vh1Vertex, vh2Vertex, vh2) == 1 &&
										std::find(vertices.begin(), vertices.end(), vh1Vertex) == vertices.end() &&
										std::find(vertices.begin(), vertices.end(), vh2Vertex) == vertices.end()) {

										realQuadQuality = getQuadQualityFactor(theMesh, vh1, vh1Vertex, vh2Vertex, vh2);
										if (realQuadQuality > toQuadQuality) {
											toQuadQuality = realQuadQuality;
											toQuadVertices = { vh1, vh1Vertex, vh2Vertex, vh2 };
										}
									}
								}
							}
						}
					}
					if (toQuadVertices.size() == 4) {
						deleteTriaInQuad(theMesh, toQuadVertices[0], toQuadVertices[1], toQuadVertices[2], toQuadVertices[3]);
						
						autoAddFace(theMesh, toQuadVertices[0], toQuadVertices[3], toQuadVertices[2], toQuadVertices[1], quadHandle);

						PolyMesh::HalfedgeHandle halfEdgeHandle = theMesh.find_halfedge(toQuadVertices[0], toQuadVertices[3]);

						PolyMesh::EdgeHandle newEdge = theMesh.edge_handle(halfEdgeHandle);
						
						std::replace(boundaryEdges.begin(), boundaryEdges.end(), currentEdge, newEdge);
						std::replace(currentEdges.begin(), currentEdges.end(), currentEdge, newEdge);
						std::replace(processedEdges.begin(), processedEdges.end(), currentEdge, newEdge);

						currentEdge = newEdge;

						quadFlag = 1;
					}
				}
			}

			// single tria currentEdge
			if (quadFlag == 0) {
				int vh1TriaNum = 0;
				for (PolyMesh::VertexFaceIter vf_it = theMesh.vf_iter(vh1); vf_it.is_valid(); ++vf_it) {
					if (theMesh.valence(*vf_it) == 3) {
						vh1TriaNum++;
					}
				}
				int vh2TriaNum = 0;
				for (PolyMesh::VertexFaceIter vf_it = theMesh.vf_iter(vh2); vf_it.is_valid(); ++vf_it) {
					if (theMesh.valence(*vf_it) == 3) {
						vh2TriaNum++;
					}
				}


				if (vh1TriaNum == 1) {

					// 先找到当前edge所对应的三角形面
					PolyMesh::FaceHandle curTria;
					for (PolyMesh::VertexFaceIter vf_it = theMesh.vf_iter(vh1); vf_it.is_valid(); ++vf_it) {
						if (theMesh.valence(*vf_it) == 3) {
							curTria = *vf_it;
							break;
						}
					}

					// 获取当前三角形面的三个节点
					std::vector<PolyMesh::VertexHandle> triaVertices;
					for (PolyMesh::FaceVertexIter fv_it = theMesh.fv_iter(curTria); fv_it.is_valid(); ++fv_it) {
						triaVertices.push_back(*fv_it);
					}
					// 从当前当前三角形面的三个节点中去除当前节点
					triaVertices.erase(std::remove(triaVertices.begin(), triaVertices.end(), vh1), triaVertices.end());

					// 获取剩下的节点所在的三角形面
					std::vector<PolyMesh::FaceHandle> leftVHtrias;
					for (PolyMesh::VertexFaceIter vf_it = theMesh.vf_iter(triaVertices[0]); vf_it.is_valid(); ++vf_it) {
						if (theMesh.valence(*vf_it) == 3) {
							leftVHtrias.push_back(*vf_it);
						}
					}
					std::vector<PolyMesh::FaceHandle> adjTriaFaces_new;
					for (PolyMesh::VertexFaceIter vf_it = theMesh.vf_iter(triaVertices[1]); vf_it.is_valid(); ++vf_it) {
						if (theMesh.valence(*vf_it) == 3 &&
							std::find(leftVHtrias.begin(), leftVHtrias.end(), *vf_it) != leftVHtrias.end())
						{
							adjTriaFaces_new.push_back(*vf_it);
						}
					}

					for (auto triaFace_new : adjTriaFaces_new) {
						// 从所在的三角形面中找到当前三角形面关于剩下的节点对称的三角形面
						std::set<PolyMesh::VertexHandle> triasVertices;
						for (PolyMesh::FaceVertexIter fv_it = theMesh.fv_iter(curTria); fv_it.is_valid(); ++fv_it) {
							triasVertices.insert(*fv_it);
						}
						for (PolyMesh::FaceVertexIter fv_it = theMesh.fv_iter(triaFace_new); fv_it.is_valid(); ++fv_it) {
							triasVertices.insert(*fv_it);
						}
						triasVertices.insert(triaVertices[0]);
						triasVertices.insert(triaVertices[1]);
						if (triasVertices.size() == 4) {
							std::vector<PolyMesh::VertexHandle> triaVertices_new;
							for (PolyMesh::FaceVertexIter fv_it = theMesh.fv_iter(triaFace_new); fv_it.is_valid(); ++fv_it) {
								triaVertices_new.push_back(*fv_it);
							}
							triaVertices_new.erase(std::remove(triaVertices_new.begin(), triaVertices_new.end(), triaVertices[0]), triaVertices_new.end());
							triaVertices_new.erase(std::remove(triaVertices_new.begin(), triaVertices_new.end(), triaVertices[1]), triaVertices_new.end());

							// 获取找到的三角形面中与当前节点对称的节点
							PolyMesh::VertexHandle reflectVertex = triaVertices_new[0];

							if (checkQuadFeasibility(theMesh, vh1, triaVertices[0], reflectVertex, triaVertices[1]) == 1 &&
								getFaceAngle(theMesh, vh1, reflectVertex, triaVertices[0], triaVertices[1]) > 90.0)
							{
								theMesh.request_vertex_status();
								theMesh.request_edge_status();
								theMesh.request_face_status();

								theMesh.delete_face(triaFace_new, false);
								theMesh.delete_face(curTria, false);
								theMesh.garbage_collection(false);

								autoAddFace(theMesh, vh1, triaVertices[0], reflectVertex, triaVertices[1], quadHandle);
								
								PolyMesh::HalfedgeHandle halfEdgeHandle = theMesh.find_halfedge(vh1, vh2);

								PolyMesh::EdgeHandle newEdge = theMesh.edge_handle(halfEdgeHandle);
								std::replace(boundaryEdges.begin(), boundaryEdges.end(), currentEdge, newEdge);
								std::replace(currentEdges.begin(), currentEdges.end(), currentEdge, newEdge);
								std::replace(processedEdges.begin(), processedEdges.end(), currentEdge, newEdge);
								currentEdge = newEdge;

								
								

								quadFlag = 1;

								// 获取四边形单元的节点
								std::vector<PolyMesh::VertexHandle> quadVertices = { vh1, triaVertices[0], reflectVertex, triaVertices[1] };


								if (std::find(quadVertices.begin(), quadVertices.end(), vh1_third) != quadVertices.end()) {
									PolyMesh::HalfedgeHandle thirdHalfEdgeHandle = theMesh.find_halfedge(vh1, vh1_third);
									PolyMesh::EdgeHandle thirdEdge = theMesh.edge_handle(thirdHalfEdgeHandle);
									if (std::find(processedEdges.begin(), processedEdges.end(), thirdEdge) == processedEdges.end()) {
										processedEdges.push_back(thirdEdge);
										tempEdge = thirdEdge;
									}

								}
								else if (std::find(quadVertices.begin(), quadVertices.end(), vh2_third) != quadVertices.end()) {
									PolyMesh::HalfedgeHandle thirdHalfEdgeHandle = theMesh.find_halfedge(vh2, vh2_third);
									PolyMesh::EdgeHandle thirdEdge = theMesh.edge_handle(thirdHalfEdgeHandle);
									if (std::find(processedEdges.begin(), processedEdges.end(), thirdEdge) == processedEdges.end()) {
										processedEdges.push_back(thirdEdge);
										tempEdge = thirdEdge;
									}
								}


								break;
							}
						}
					}
				}
				else if (vh2TriaNum == 1) {
					// 先找到当前edge所对应的三角形面
					PolyMesh::FaceHandle curTria;
					for (PolyMesh::VertexFaceIter vf_it = theMesh.vf_iter(vh2); vf_it.is_valid(); ++vf_it) {
						if (theMesh.valence(*vf_it) == 3) {
							curTria = *vf_it;
							break;
						}
					}

					// 获取当前三角形面的三个节点
					std::vector<PolyMesh::VertexHandle> triaVertices;
					for (PolyMesh::FaceVertexIter fv_it = theMesh.fv_iter(curTria); fv_it.is_valid(); ++fv_it) {
						triaVertices.push_back(*fv_it);
					}
					// 从当前当前三角形面的三个节点中去除当前节点
					triaVertices.erase(std::remove(triaVertices.begin(), triaVertices.end(), vh2), triaVertices.end());

					// 获取剩下的节点所在的三角形面
					std::vector<PolyMesh::FaceHandle> leftVHtrias;
					for (PolyMesh::VertexFaceIter vf_it = theMesh.vf_iter(triaVertices[0]); vf_it.is_valid(); ++vf_it) {
						if (theMesh.valence(*vf_it) == 3) {
							leftVHtrias.push_back(*vf_it);
						}
					}
					std::vector<PolyMesh::FaceHandle> adjTriaFaces_new;
					for (PolyMesh::VertexFaceIter vf_it = theMesh.vf_iter(triaVertices[1]); vf_it.is_valid(); ++vf_it) {
						if (theMesh.valence(*vf_it) == 3 &&
							std::find(leftVHtrias.begin(), leftVHtrias.end(), *vf_it) != leftVHtrias.end())
						{
							adjTriaFaces_new.push_back(*vf_it);
						}
					}

					for (auto triaFace_new : adjTriaFaces_new) {
						// 从所在的三角形面中找到当前三角形面关于剩下的节点对称的三角形面
						std::set<PolyMesh::VertexHandle> triasVertices;
						for (PolyMesh::FaceVertexIter fv_it = theMesh.fv_iter(curTria); fv_it.is_valid(); ++fv_it) {
							triasVertices.insert(*fv_it);
						}
						for (PolyMesh::FaceVertexIter fv_it = theMesh.fv_iter(triaFace_new); fv_it.is_valid(); ++fv_it) {
							triasVertices.insert(*fv_it);
						}
						triasVertices.insert(triaVertices[0]);
						triasVertices.insert(triaVertices[1]);
						if (triasVertices.size() == 4) {
							std::vector<PolyMesh::VertexHandle> triaVertices_new;
							for (PolyMesh::FaceVertexIter fv_it = theMesh.fv_iter(triaFace_new); fv_it.is_valid(); ++fv_it) {
								triaVertices_new.push_back(*fv_it);
							}
							triaVertices_new.erase(std::remove(triaVertices_new.begin(), triaVertices_new.end(), triaVertices[0]), triaVertices_new.end());
							triaVertices_new.erase(std::remove(triaVertices_new.begin(), triaVertices_new.end(), triaVertices[1]), triaVertices_new.end());

							// 获取找到的三角形面中与当前节点对称的节点
							PolyMesh::VertexHandle reflectVertex = triaVertices_new[0];
							
							// 
							if (checkQuadFeasibility(theMesh, vh2, triaVertices[0], reflectVertex, triaVertices[1]) == 1 &&
								getFaceAngle(theMesh, vh2, reflectVertex, triaVertices[0], triaVertices[1]) > 90.0)
							{
								theMesh.request_vertex_status();
								theMesh.request_edge_status();
								theMesh.request_face_status();

								theMesh.delete_face(triaFace_new, false);
								theMesh.delete_face(curTria, false);
								theMesh.garbage_collection(false);
								
								autoAddFace(theMesh, vh2, triaVertices[0], reflectVertex, triaVertices[1], quadHandle);

								PolyMesh::HalfedgeHandle halfEdgeHandle = theMesh.find_halfedge(vh1, vh2);

								PolyMesh::EdgeHandle newEdge = theMesh.edge_handle(halfEdgeHandle);
								std::replace(boundaryEdges.begin(), boundaryEdges.end(), currentEdge, newEdge);
								std::replace(currentEdges.begin(), currentEdges.end(), currentEdge, newEdge);
								std::replace(processedEdges.begin(), processedEdges.end(), currentEdge, newEdge);

								currentEdge = newEdge;

								
								

								quadFlag = 1;

								// 获取四边形单元的节点
								std::vector<PolyMesh::VertexHandle> quadVertices = { vh2, triaVertices[0], reflectVertex, triaVertices[1] };


								if (std::find(quadVertices.begin(), quadVertices.end(), vh1_third) != quadVertices.end()) {
									PolyMesh::HalfedgeHandle thirdHalfEdgeHandle = theMesh.find_halfedge(vh1, vh1_third);
									PolyMesh::EdgeHandle thirdEdge = theMesh.edge_handle(thirdHalfEdgeHandle);
									if (std::find(processedEdges.begin(), processedEdges.end(), thirdEdge) == processedEdges.end())
									{
										processedEdges.push_back(thirdEdge);
										tempEdge = thirdEdge;
									}
								}
								else if (std::find(quadVertices.begin(), quadVertices.end(), vh2_third) != quadVertices.end()) {
									PolyMesh::HalfedgeHandle thirdHalfEdgeHandle = theMesh.find_halfedge(vh2, vh2_third);
									PolyMesh::EdgeHandle thirdEdge = theMesh.edge_handle(thirdHalfEdgeHandle);
									if (std::find(processedEdges.begin(), processedEdges.end(), thirdEdge) == processedEdges.end())
									{
										processedEdges.push_back(thirdEdge);
										tempEdge = thirdEdge;
									}
								}

								
								break;
							}
						}
					}
				}
				/*if (vh1TriaNum == 1 && vh2TriaNum == 1 && quadFlag == 0) {
					break;
				}*/
			}

			// qmorph方法
			if (quadFlag == 0) {
				PolyMesh::VertexHandle vh1Vertex = getAlignVertex(theMesh, vh1, vh2, vh1_third);
				PolyMesh::VertexHandle vh2Vertex = getAlignVertex(theMesh, vh2, vh1, vh2_third);

#ifdef _DEBUG
				if (theMesh.is_valid_handle(vh1Vertex)) {
					PolyMesh::Point vh1Vertex_point = theMesh.point(vh1Vertex);
					std::cout << "vh1Vertex_point : " << vh1Vertex_point << std::endl;
				}
				if (theMesh.is_valid_handle(vh2Vertex)) {
					PolyMesh::Point vh2Vertex_point = theMesh.point(vh2Vertex);

					std::cout << "vh1Vertex_point : " << vh2Vertex_point << std::endl;
				}

				saveToVtkFile(theMesh, "swapsplit_after.vtk");
#endif // _DEBUG	

				if (vh1Vertex != vh2Vertex && 
					theMesh.is_valid_handle(vh1Vertex) && 
					theMesh.is_valid_handle(vh2Vertex) &&
					checkRecoveryFeasibility(theMesh, vh1Vertex, vh2Vertex)) 
				{
					if (checkQuadFeasibility(theMesh, vh1, vh1Vertex, vh2Vertex, vh2)== 1) {
						if (edgeRecoverySequence(theMesh, vh1Vertex, vh2Vertex)) {
#ifdef _DEBUG
							saveToVtkFile(theMesh, "edgerecovery.vtk");
#endif // _DEBUG
							deleteTriaInQuad(theMesh, vh1, vh1Vertex, vh2Vertex, vh2);

							autoAddFace(theMesh, vh1, vh1Vertex, vh2Vertex, vh2, quadHandle);

							mergeBadTriaByQuad(theMesh, quadHandle, boundaryVertices);
							PolyMesh::HalfedgeHandle halfEdgeHandle = theMesh.find_halfedge(vh1, vh2);

							PolyMesh::EdgeHandle newEdge = theMesh.edge_handle(halfEdgeHandle);
							std::replace(boundaryEdges.begin(), boundaryEdges.end(), currentEdge, newEdge);
							std::replace(currentEdges.begin(), currentEdges.end(), currentEdge, newEdge);
							std::replace(processedEdges.begin(), processedEdges.end(), currentEdge, newEdge);
							currentEdge = newEdge;
							quadFlag = 1;
						}
					}
					else {
						if (checkVetexConnection(theMesh, vh1Vertex, vh2Vertex) && connectFlag == 0 &&
							std::find(boundaryVertices.begin(), boundaryVertices.end(), vh1Vertex) != boundaryVertices.end() &&
							std::find(boundaryVertices.begin(), boundaryVertices.end(), vh2Vertex) != boundaryVertices.end() &&
							std::find(vertices.begin(), vertices.end(), vh1Vertex) == vertices.end() &&
							std::find(vertices.begin(), vertices.end(), vh2Vertex) == vertices.end()) 
						{

							theMesh.request_vertex_status();
							theMesh.request_edge_status();
							theMesh.request_face_status();

							deleteTriaInQuad(theMesh, vh1, vh1Vertex, vh2Vertex, vh2);
							
							autoAddFace(theMesh, vh1, vh1Vertex, vh2Vertex, vh2, quadHandle);

							PolyMesh::HalfedgeHandle halfEdgeHandle = theMesh.find_halfedge(vh1, vh2);

							PolyMesh::EdgeHandle newEdge = theMesh.edge_handle(halfEdgeHandle);
							std::replace(boundaryEdges.begin(), boundaryEdges.end(), currentEdge, newEdge);
							std::replace(currentEdges.begin(), currentEdges.end(), currentEdge, newEdge);
							std::replace(processedEdges.begin(), processedEdges.end(), currentEdge, newEdge);
							currentEdge = newEdge;
							connectFlag = 1;
							quadFlag = 1;
						}
					}
				}
				
				if (quadFlag == 1) {
					if (vh1Vertex == vh1_third) {
						PolyMesh::HalfedgeHandle vh1halfEH = theMesh.find_halfedge(vh1, vh1Vertex);
						if (theMesh.is_valid_handle(vh1halfEH)) {
							PolyMesh::EdgeHandle vh1TempEdge = theMesh.edge_handle(vh1halfEH);

							processedEdges.push_back(vh1TempEdge);
							tempEdge = vh1TempEdge;
						}
					}

					if (vh2Vertex == vh2_third) {
						PolyMesh::HalfedgeHandle vh2halfEH = theMesh.find_halfedge(vh2, vh2Vertex);
						if (theMesh.is_valid_handle(vh2halfEH)) {
							PolyMesh::EdgeHandle vh2TempEdge = theMesh.edge_handle(vh2halfEH);

							processedEdges.push_back(vh2TempEdge);
							tempEdge = vh2TempEdge;
						}
					}
				}
			}
			
			// theMesh.update_normals();
			if (theMesh.is_valid_handle(tempEdge)) {
				heh1 = theMesh.halfedge_handle(tempEdge, 0);
				heh2 = theMesh.halfedge_handle(tempEdge, 1);
				vh1 = theMesh.to_vertex_handle(heh1);
				vh2 = theMesh.to_vertex_handle(heh2);
			}


			std::set<PolyMesh::EdgeHandle> edgeSet;
			for (PolyMesh::VertexEdgeIter ve_it = theMesh.ve_iter(vh1); ve_it.is_valid(); ++ve_it) {
				if (std::find(currentEdges.begin(), currentEdges.end(), *ve_it) != currentEdges.end()) {
					edgeSet.insert(*ve_it);
				}
			}
			for (PolyMesh::VertexEdgeIter ve_it = theMesh.ve_iter(vh2); ve_it.is_valid(); ++ve_it) {
				if (std::find(currentEdges.begin(), currentEdges.end(), *ve_it) != currentEdges.end()) {
					edgeSet.insert(*ve_it);
				}
			}
			std::vector<PolyMesh::EdgeHandle> edgeVector;
			for (auto eh : edgeSet) {
				if (std::find(processedEdges.begin(), processedEdges.end(), eh) == processedEdges.end()) {
					edgeVector.push_back(eh);
				}
			}
			if (edgeVector.size() == 0) {
				break;
			}
			else {
				currentEdge = edgeVector[0];
			}
#ifdef _DEBUG
			// saveToVtkFile(theMesh, fileName);
#endif // _DEBUG
		}

#ifdef _DEBUG
		std::cout << ".....current edge loop finish..." << std::endl;
#endif // _DEBUG
		mergeSingleTriaNew(theMesh);
		combineQuadWithInteriorVertex(theMesh);
		// jacobiLaplaceSmooth(theMesh);
#ifdef _DEBUG	
		updateVtkView(thePoints, thePolyData, theRender, theMesh);
		saveToVtkFile(theMesh, fileNameInput);
#endif // _DEBUG
	}


#ifdef _DEBUG	
	saveToVtkFile(theMesh, "close_before.vtk");
#endif // _DEBUG	
	closeDiamondQuad(theMesh, thePoints, thePolyData, theRender);
#ifdef _DEBUG
	saveToVtkFile(theMesh, "adjust_before.vtk");
#endif // _DEBUG	
	mergeIstyleQuad(theMesh, thePoints, thePolyData, theRender);
	closeDiamondQuad(theMesh, thePoints, thePolyData, theRender);
	mergeSplitedTria(theMesh);
}







int main() {

	TopoDS_Shape shape;
	if (!BRepTools::Read(shape, "E:/docs/ANC101_2.brep", BRep_Builder())) {
		std::cout << "Failed to read BREP shape from file" << std::endl;
		return 1;
	}

	PolyMesh triaMesh;
	PolyMesh quadMesh;
	std::vector< PolyMesh> faceMeshes;



	TIMER_NEW
	TIMER_GO
	netGenTriangulation(shape, triaMesh, faceMeshes);
	// equivalencePolyMesh(1.0, triaMesh);
	TIMER_FINISH
	TIMER_COUT_RESULT_MSG("netgen triangulation")


	vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
	vtkSmartPointer<vtkPolyData> polydata = vtkSmartPointer<vtkPolyData>::New();
	
#ifdef _DEBUG	
	openMeshToVTK(points, polydata, triaMesh);
#endif // _DEBUG

	// 创建vtkRenderer对象
	vtkSmartPointer<vtkRenderer> renderer = vtkSmartPointer<vtkRenderer>::New();

	// 创建vtkRenderWindow对象
	vtkSmartPointer<vtkRenderWindow> renderwindow =
		vtkSmartPointer<vtkRenderWindow>::New();
	renderwindow->AddRenderer(renderer);
	renderwindow->SetSize(800, 600);

	vtkSmartPointer<vtkInteractorStyleTrackballCamera> style =
		vtkSmartPointer<vtkInteractorStyleTrackballCamera>::New();
	// 创建vtkRenderWindowInteractor对象
	vtkSmartPointer<vtkRenderWindowInteractor> interactor =
		vtkSmartPointer<vtkRenderWindowInteractor>::New();
	interactor->SetRenderWindow(renderwindow);
	interactor->SetInteractorStyle(style);

	// 创建vtkActor对象
	vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
	actor->GetProperty()->SetColor(255, 255, 0); // 设置颜色
	actor->GetProperty()->SetOpacity(0.8); // 设置透明度
	actor->GetProperty()->SetPointSize(2); // 设置点大小
	actor->GetProperty()->SetLineWidth(2); // 设置线宽
	actor->GetProperty()->SetRepresentationToWireframe();

	// 将vtkUnstructuredGrid添加到vtkActor中
	vtkSmartPointer<vtkPolyDataMapper> mapper = 
		vtkSmartPointer<vtkPolyDataMapper>::New();
	mapper->SetInputData(polydata);
	actor->SetMapper(mapper);

	// 将vtkActor添加到vtkRenderer中
	renderer->AddActor(actor);

	// 设置vtkRenderWindowInteractor并启动渲染
	interactor->Initialize();
	renderwindow->Render();


	TIMER_RESET
	TIMER_GO
	for (auto faceMesh : faceMeshes) {
#ifdef _DEBUG	
		saveToVtkFile(faceMesh, "qmorph_before.vtk");
#endif // _DEBUG			
		qmorphTriaToQuad(faceMesh, points, polydata, renderwindow);
		faceMesh.garbage_collection();
#ifdef _DEBUG
		saveToVtkFile(faceMesh, "qmorph_after.vtk");
#endif // _DEBUG		

		

		std::map<PolyMesh::VertexHandle, PolyMesh::VertexHandle> vhandle_map;

		for (auto v_it = faceMesh.vertices_begin(); v_it != faceMesh.vertices_end(); ++v_it) {
			PolyMesh::Point p = faceMesh.point(*v_it);
			PolyMesh::VertexHandle vh = quadMesh.add_vertex(p);
			vhandle_map[*v_it] = vh;
		}

		for (auto f_it = faceMesh.faces_begin(); f_it != faceMesh.faces_end(); ++f_it) {
			std::vector<PolyMesh::VertexHandle> face_vhandles;
			for (auto fv_it = faceMesh.fv_iter(*f_it); fv_it.is_valid(); ++fv_it) {
				face_vhandles.push_back(vhandle_map[*fv_it]);
			}
			quadMesh.add_face(face_vhandles);
		}
#ifdef _DEBUG			
		saveToVtkFile(quadMesh, "quadMesh.vtk");
#endif // _DEBUG	
	}


	std::map<PolyMesh::Point, PolyMesh::VertexHandle> pvMap;
	PolyMesh resultMesh;

	int triaNum = 0;
	int quadNum = 0;
	for (PolyMesh::FaceHandle fh : quadMesh.faces()) {
		std::vector< PolyMesh::VertexHandle> vhs;
		for (PolyMesh::VertexHandle vh : quadMesh.fv_range(fh)) {
			auto pos = quadMesh.point(vh);
			auto it = pvMap.find(pos);
			if (it != pvMap.end()) {
				vhs.push_back(it->second);
			}
			else {
				PolyMesh::VertexHandle vh_tmp = resultMesh.add_vertex(pos);
				pvMap[pos] = vh_tmp;
				vhs.push_back(vh_tmp);
			}
		}
		resultMesh.add_face(vhs);
		if (quadMesh.valence(fh) == 3) {
			triaNum++;
		}
		else {
			quadNum++;
		}
	}
	jacobiLaplaceSmooth(resultMesh);
	saveToVtkFile(resultMesh, "resultMesh.vtk");

	std::cout << std::endl;
	std::cout << std::endl;
	std::cout << std::endl;
	std::cout << "---------------------------------------------" << std::endl; 
	std::cout << "---------------------------------------------" << std::endl;
	std::cout << "total number of vertices : " << pvMap.size() << std::endl;
	std::cout << "total number of tria faces : " << triaNum << std::endl;
	std::cout << "total number of quad faces : " << quadNum << std::endl;

	
	updateVtkView(points, polydata, renderwindow, resultMesh);
	TIMER_FINISH
	TIMER_COUT_RESULT_MSG("qmorph to quad")

	interactor->Start();
	return EXIT_SUCCESS;
}





